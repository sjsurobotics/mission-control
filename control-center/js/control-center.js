"use strict";

(function ($) {
  $('.spinner .btn:first-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) + 1);
  });
  $('.spinner .btn:last-of-type').on('click', function() {
    $('.spinner input').val( parseInt($('.spinner input').val(), 10) - 1);
  });
})(jQuery);

var voltage = new JustGage({
    id: "voltage",
    title : "Voltage",
    titleFontColor : "#fff",
    valueFontColor  : "#fff",
    label: "Volts",
    value : 0,
    min: 24.0,
    max: 28.9,
    decimals: 2,
    levelColors: [  "#ff0000", "#F27C07",  "#a9d70b" ],
    gaugeWidthScale: 0.6
});

var signal = new Signal(document.querySelector("#signal"));
// set bar strength
signal.setStrength(0);

var feedback = {
	log: function(str, obj) {
		if(typeof obj == "undefined") {
			console.log(str);
			$("#log").prepend(str+"\n");
		} else {
			console.log(str, obj);
			$("#log").prepend(str+" "+JSON.stringify(obj)+"\n");
		}
		// keep the log on the bottom
		//var textarea = document.getElementById('log');
		//textarea.scrollTop = textarea.scrollHeight;
	}
};
var entity;
var socket = {
	emit: function() {
		alert("NOT CONNECTED TO A SERVER YET!");
	}
};
var registered = false;
//// DOM Event listeners
$("#connect").on("click", function() {
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', handleOcularSignal);
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#core_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	socket.emit("REGISTER", {
		entity: "captain",
		password: "destroymit"
	});
});

$("#all-views").on("click", function() {
	window.open('omni-view.html');
	window.open('tracker-view.html');
	window.open('map-view.html');
});

$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		// //socket.disconnect();
		// socket.io.close();
		// socket = null;
		// socket = undefined;
		// registered = false;
		location.reload();
	}
});
$("#ctrl").on("click", function() {
	if(!registered) { return; }
	//TODO: remove this when ready!
	return;
	var directive = $("#directive").val();
	//var info = editor.get();
	if(info == "") {
		socket.emit("CTRLSIG", { directive: directive });
	} else {
		socket.emit("CTRLSIG", { directive: directive, info: info });
	}
});
$("#directive").on("change", function() {
	editor.set(schemas[$(this).val()]);
});
// Prevent the backspace key from navigating back.
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
});

function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case 'NOTCONNECTED':
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

var sensors = {
	degrees: 0,
	voltage: 0,
	temp: 0,
	strength: -1,
	bars: 0,
};

var compass = new Compass("compass");

/*
var data = {
	directive: "SENSORS",
	info: {
		compass: {
			heading: 2,
		}
	}
};
*/
//data = '{"directive": "SENSORS","info": {"compass": {"heading": 2}}}'
function handleRoverSignal(data) {
	//feedback.log("INCOMING ROVERSIG", data);
	//console.log(data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			console.log(info);
			try {
				sensors["degrees"] = (_.isUndefined(info["compass"]["heading"]) ? 0 : info["compass"]["heading"]);
				sensors["voltage"] = (_.isUndefined(info["power"]["voltage"]) ? 0 : info["power"]["voltage"]);
				sensors["temp"] = (_.isUndefined(info["temperature"]["cpu"]) ? 0 : info["temperature"]["cpu"]);
				sensors["strength"] = (_.isUndefined(info["signal"]["strength"]) ? 0 : info["signal"]["strength"]);;
				sensors["bars"] = (_.isUndefined(info["signal"]["bars"]) ? 0 : info["signal"]["bars"]);;
			} catch(e) {
				console.log(e);
			}
			break;
		case "OCULARSIG":
			//black -> white text @ 39%
			if(!_.isUndefined(data[""])) {
			} else {
				feedback.log("INCOMING ROVERSIG", data);
			}
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}
}
function handleOcularSignal(data) {
	//feedback.log("INCOMING OCULARSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			//Max = 65C, Min = 25C
			//black -> white text @ 39%
			var bar_ratio = (data["info"]["cputemp"]-25)/(65-25);
			$("#"+data["info"]["device"]+"-temp").width((100*bar_ratio)+"%");
			if(data["info"]["cputemp"] > 43) { // 55C
				$("#"+data["info"]["device"]+"-temp").addClass("progress-bar-danger");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-warning");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-success");
			} else if(data["info"]["cputemp"] > 41) { // 42C
				$("#"+data["info"]["device"]+"-temp").addClass("progress-bar-warning");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-danger");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-success");
			} else if(data["info"]["cputemp"] < 40.5) { // 42C
				$("#"+data["info"]["device"]+"-temp").addClass("progress-bar-success");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-danger");
				$("#"+data["info"]["device"]+"-temp").removeClass("progress-bar-warning");
			}
			$("#"+data["info"]["device"]+"-temp").html(data["info"]["cputemp"]+"&deg;C "+data["info"]["device"]+" CPU");
			break;
		default:
			feedback.log("INCOMING OCULARSIG", data);
			break;
	}
}
setInterval(function() {
	voltage.refresh(sensors["voltage"]);
}, 5000);
setInterval(function() {
	compass.update(sensors["degrees"]);
}, 250);
setInterval(function() {
	//Max = 65C, Min = 25C
	//black -> white text @ 39%
	var bar_ratio = (sensors["temp"]-25)/(65-25);
	$("#cortex-temp").width((100*bar_ratio)+"%");
	if(sensors["temp"] > 43) { // 55C
		$("#cortex-temp").addClass("progress-bar-danger");
		$("#cortex-temp").removeClass("progress-bar-warning");
		$("#cortex-temp").removeClass("progress-bar-success");
	} else if(sensors["temp"] > 41) { // 42C
		$("#cortex-temp").addClass("progress-bar-warning");
		$("#cortex-temp").removeClass("progress-bar-danger");
		$("#cortex-temp").removeClass("progress-bar-success");
	} else if(sensors["temp"] < 40.5) { // 42C
		$("#cortex-temp").addClass("progress-bar-success");
		$("#cortex-temp").removeClass("progress-bar-danger");
		$("#cortex-temp").removeClass("progress-bar-warning");
	}
	$("#cortex-temp").html(sensors["temp"]+"&deg;C cortex CPU");
}, 300);
setInterval(function() {
	signal.setStrength(sensors["bars"]);
	$("#signal-strength").html(sensors["strength"]+"dBm");
}, 2000);

//// Control Panel ////
var standards = {
	omni: {
		view: "",
		res: 400,
		//width: 864,
		//height: 480,
		// width: 600,
		// height: 480,
		width: 432,
		height: 240,
		stream: 0
	},
	tracker: {
		view: "",
		res: 500,
		stream: 1
	},
	chassis: {
		mic: 0,
		bitrate: 96,
		stream: 0
	}
};

//// Tracker Audio Stream
// $("#audio-tracker-res").on('switchChange.bootstrapSwitch', function(event, state) {
// 	if(state) {
// 		standards["atracker"]["bitrate"] = 192;	
// 	} else {
// 		standards["atracker"]["bitrate"] = 96;
// 	}
// }).bootstrapSwitch({
// 	onText: "HD",
// 	offText: "SD",
// 	handleWidth: 50,
// 	size: "small"
// });
// $("#audio-tracker-switch").on('switchChange.bootstrapSwitch', function(event, state) {
// 	if(state) {
// 		socket.emit("CTRLSIG", { directive: "AUDIO", info: standards["atracker"] });
// 	} else {
// 		socket.emit("CTRLSIG", { directive: "AUDIO", info: { mic: "off", stream: 1 } });
// 	}
// }).bootstrapSwitch({
// 	handleWidth: 100,
// 	labelText: "Tracker Audio",
// 	size: "small"
// });

$("#omni-slider").slider({
    ticks: [0, 1, 2, 3],
    value: 3,
    ticks_labels: ['640x480', '864x480', '960x720', '1024x576'],
    ticks_snap_bounds: 100
});
$("#omni-slider").on("slide", function(curr) {
	var lwidth = [640, 864, 960, 1024];
	var lheight = [480, 480, 720, 576];
	console.log(curr.value);
	standards["omni"]["width"] = lwidth[curr.value];
	standards["omni"]["height"] = lheight[curr.value];
});
$("#tracker-slider").slider({
    ticks: [0, 1, 2, 3],
    value: 3,
    ticks_labels: ['640x480', '864x480', '960x720', '1024x576'],
    ticks_snap_bounds: 100
});
$("#tracker-slider").on("slide", function(curr) {
	var lwidth = [640, 864, 960, 1024];
	var lheight = [480, 480, 720, 576];
	console.log(curr.value);
	standards["tracker"]["width"] = lwidth[curr.value];
	standards["tracker"]["height"] = lheight[curr.value];
});

$("#kill-switch-a2").on('click', function() {
	$("#audio-tracker-switch").bootstrapSwitch('state', false);
});

//// OMNI video stream
$("#video-omni-res").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["omni"]["res"] = 800;
	} else {
		standards["omni"]["res"] = 400;
	}
}).bootstrapSwitch({
	onText: "HD",
	offText: "SD",
	labelText: "(A)",
	handleWidth: 50,
	size: "small"
});

$("#nav-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["omni"]["view"] = "navi";
		socket.emit("CTRLSIG", { directive: "VIDEO", info: standards["omni"] });
	} else {
		socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "off", stream: 0 } });
	}
}).bootstrapSwitch({
	handleWidth: 110,
	labelText: "Navi Video (S)",
	size: "small"
});
$("#arm-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["omni"]["view"] = "arm";
		socket.emit("CTRLSIG", { directive: "VIDEO", info: standards["omni"] });
	} else {
		socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "off", stream: 0 } });
	}
}).bootstrapSwitch({
	handleWidth: 110,
	labelText: "Arm Video (D)",
	size: "small"
});
$("#hull-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["omni"]["view"] = "hull";
		socket.emit("CTRLSIG", { directive: "VIDEO", stream: 0, info: standards["omni"] });
	} else {
		socket.emit("CTRLSIG", { directive: "VIDEO", stream: 0, info: { view: "off", stream: 0 } });
	}
}).bootstrapSwitch({
	handleWidth: 110,
	labelText: "Hull Video (F)",
	size: "small"
});

//// Tracker Video Stream
$("#video-tracker-res").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["tracker"]["res"] = 500;	
	} else {
		standards["tracker"]["res"] = 1000;
	}
}).bootstrapSwitch({
	onText: "HD",
	offText: "SD",
	labelText: "(J)",
	handleWidth: 50,
	size: "small"
});
$("#tracker-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["tracker"]["view"] = "tracker";
		socket.emit("CTRLSIG", { directive: "VIDEO", info: standards["tracker"] });
	} else {
		socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "off", stream: 1 } });
	}
}).bootstrapSwitch({
	handleWidth: 120,
	labelText: "Tracker Video (K)",
	size: "small"
});

//// Chassis Audio Stream
$("#audio-chassis-res").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["chassis"]["bitrate"] = 192;	
	} else {
		standards["chassis"]["bitrate"] = 96;
	}
}).bootstrapSwitch({
	onText: "HD",
	offText: "SD",
	labelText: "(G)",
	handleWidth: 50,
	size: "small"
});
$("#audio-chassis-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		standards["mic"] = $("#mic").val();
		socket.emit("CTRLSIG", { directive: "AUDIO", info: standards["chassis"] });
	} else {
		socket.emit("CTRLSIG", { directive: "AUDIO", info: { mic: "off", stream: 0 } });
	}
}).bootstrapSwitch({
	handleWidth: 120,
	labelText: "Chassis Audio (H)",
	size: "small"
});

//// Control Panel
$("#kill-switch").on('click', function() {
	$("#nav-switch").bootstrapSwitch('state', false);
	$("#arm-switch").bootstrapSwitch('state', false);
	$("#hull-switch").bootstrapSwitch('state', false);
	$("#video-tracker-res").bootstrapSwitch('state', false);
	$("#tracker-switch").bootstrapSwitch('state', false);
	$("#audio-chassis-res").bootstrapSwitch('state', false);
	$("#audio-chassis-switch").bootstrapSwitch('state', false);
	$("#audio-tracker-res").bootstrapSwitch('state', false);
	socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "killall", stream: 0 } });
	socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "killall", stream: 1 } });
});

$("#force-kill-switch").on('click', function() {
	$("#nav-switch").bootstrapSwitch('state', false);
	$("#arm-switch").bootstrapSwitch('state', false);
	$("#hull-switch").bootstrapSwitch('state', false);
	$("#video-tracker-res").bootstrapSwitch('state', false);
	$("#tracker-switch").bootstrapSwitch('state', false);
	$("#audio-chassis-res").bootstrapSwitch('state', false);
	$("#audio-chassis-switch").bootstrapSwitch('state', false);
	$("#audio-tracker-res").bootstrapSwitch('state', false);
	socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "force-killall", stream: 0 } });
	socket.emit("CTRLSIG", { directive: "VIDEO", info: { view: "force-killall", stream: 1 } });
});
$("#restart-oculus-1").on('click', function() {
	socket.emit("CTRLSIG", { directive: "OCULUS1", info: { signal: "RESTART", stream: 0 } });
});
$("#restart-oculus-2").on('click', function() {
	socket.emit("CTRLSIG", { directive: "OCULUS2", info: { signal: "RESTART", stream: 1 } });
});
$("#restart-cortex").on('click', function() {
	socket.emit("CTRLSIG", { directive: "CORTEX", info: { directive: "RESTART" } });
});
$("#restart-aux").on('click', function() {
	socket.emit("CTRLSIG", { directive: "CORTEX", info: { directive: "RESTART-AUX" } });
});
$("#restart-tracker").on('click', function() {
	socket.emit("CTRLSIG", { directive: "CORTEX", info: { directive: "RESTART-TRACKER" } });
});


$("#mast-switch").on('switchChange.bootstrapSwitch', function(event, state) {
	if(state) {
		socket.emit("CTRLSIG", { directive: "SENSOR", info: "MAST-UP" });	
	} else {
		socket.emit("CTRLSIG", { directive: "SENSOR", info: "MAST-DOWN" });
	}
}).bootstrapSwitch({
	onText: "UP",
	offText: "DOWN",
	handleWidth: 120,
	labelText: "Mast (Z)",
	size: "small"
});

$("body").keyup(function( event ) {
	var keys = {
		65 : 'a',83 : 's',68 : 'd',70 : 'f',71 : 'g',72 : 'h',
		74 : 'j',75 : 'k',76 : 'l',90 : 'z',88 : 'x',67 : 'c',
		86 : 'v',66 : 'b',78 : 'n',77 : 'm',188: '<'
	};
	var key = keys[event.keyCode];
	if(_.isUndefined(key) || !event.shiftKey) { 
		console.warn(event.keyCode);
		return; 
	}
	switch(key) {
		// Video Feed One
		case 'a':
			$("#video-omni-res").bootstrapSwitch('toggleState');
			break;
		case 's':
			$("#nav-switch").bootstrapSwitch('toggleState');
			break;
		case 'd':
			$("#arm-switch").bootstrapSwitch('toggleState');
			break;
		case 'f':
			$("#hull-switch").bootstrapSwitch('toggleState');
			break;
		// Audio Feed
		case 'g':
			$("#audio-chassis-res").bootstrapSwitch('toggleState');
			break;
		case 'h':
			$("#audio-chassis-switch").bootstrapSwitch('toggleState');
			break;
		// Video Feed Two
		case 'j':
			$("#video-tracker-res").bootstrapSwitch('toggleState');
			break;
		case 'k':
			$("#tracker-switch").bootstrapSwitch('toggleState');
			break;
		// Mast
		case 'z':
			$("#mast-switch").bootstrapSwitch('toggleState');
			break;
		// Restarts
		case 'x':
			$("#kill-switch").click();
			break;
		case 'c':
			$("#force-kill-switch").click();
			break;
		case 'v':
			$("#restart-oculus-1").click();
			break;
		case 'b':
			$("#restart-oculus-2").click();
			break;
		case 'n':
			$("#restart-cortex").click();
			break;
		case 'm':
			$("#restart-aux").click();
			break;
		case '<':
			$("#restart-tracker").click();
			break;
		default:
			break;
	}
}).keydown(function( event ) {
	if ( event.which == 13 || event.which == 16) {
		event.preventDefault();
	}
});