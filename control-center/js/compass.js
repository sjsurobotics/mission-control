function Compass(canvas_id) {
	// Global variable
	this.compass = null,
	this.needle = null,
	this.ctx = null,
	this.degrees = 0,
	this.cwidth = 200,
	this.cheight = 200;

	// Grab the compass element
	var canvas = document.getElementById(canvas_id);
	var parent = this;
	// Canvas supported?
	if (canvas.getContext('2d')) {
		this.ctx = canvas.getContext('2d');

		// Load the needle image
		this.needle = new Image();
		this.needle.src = 'images/needle.png';

		// Load the compass image
		this.compass = new Image();
		this.compass.src = 'images/compass-2.png';
		this.compass.onload = function() {
			parent.loaded();
		};
	} else {
		console.log("Canvas not supported!");
	}
}
Compass.prototype.draw = function() {
	this.ctx.clearRect(0, 0, this.cwidth, this.cheight);
	// Draw the compass onto the canvas
	this.ctx.drawImage(this.compass, 0, 0);
	this.ctx.save();
	this.ctx.translate( this.cwidth/2-5, this.cheight/2 );
	this.ctx.rotate(this.degrees * (Math.PI / 180));
	this.ctx.drawImage( this.needle, -(this.cwidth/2), -this.cheight/2-5 );
	this.ctx.restore();
	// Increment the angle of the needle by 5 degrees
	//this.degrees += 1;
};
Compass.prototype.loaded = function() {
	var parent = this;
	setInterval(function() {
		parent.draw();
	}, 100);
	this.draw();
};
Compass.prototype.update = function(deg) {
	this.degrees = deg;
	this.draw();
};