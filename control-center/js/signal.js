function Signal(elem) {
	this._elem = elem;
	this._bars = [];
	for (var i = 0; i < 5; i++) {
		var bar = document.createElement("div");
		if (i < 4) {
			bar.style.marginRight = '4%';
		}
		bar.style.width = (1/5*99 - (4/5*4)) + '%';
		bar.style.height = (i+1)/5*100 + '%';
		bar.style.background = 'rgba(0, 0, 0, 0.25)';
		bar.style.display = 'inline-block';
		elem.appendChild(bar);
		this._bars[i] = bar;
	}
}

// send through a number between 0 and 5.
Signal.prototype.setStrength = function(strength) {
	for (var i = 0; i < 5; i++) {
		if (i < strength) {
			this._bars[i].style.background = 'rgb(39, 144, 182)';
		} else {
			this._bars[i].style.background = 'rgba(0, 0, 0, 0.25)';
		}
	}
}