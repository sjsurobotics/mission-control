"USE STRICT"
/*var current = new JustGage({
    id: "current",
    title : "Current",
    titleFontColor : "#fff",
    valueFontColor  : "#fff",
    value : 5.25,
    label: "Amps",
    min: 0,
    max: 18,
    decimals: 2,
    gaugeWidthScale: 0.6
});
var voltage = new JustGage({
    id: "voltage",
    title : "Charge",
    titleFontColor : "#fff",
    valueFontColor  : "#fff",
    label: "%",
    value : 70,
    min: 0,
    max: 100,
    decimals: 2,
    gaugeWidthScale: 0.6
});*/

var textarea = document.getElementById('log');
var feedback = {
    log: function(str, obj) {
        if(typeof obj == "undefined" || typeof obj == undefined) {
            //console.log(str);
            $("#log").append(str+"\n");
        } else {
            //console.log(str, obj);
            $("#log").append(str+" "+JSON.stringify(obj)+"\n");
        }
        textarea.scrollTop = textarea.scrollHeight;
    }
};

$("#m1, #m2, #m3, #m4, #m5, #m6").slider('setValue', 0).on('slide', function() {
    if(parseInt($("#" + this.id).val() ) >= 0) {
        indivMotor.motor[this.id].direction = "forward";
    } else {
         indivMotor.motor[this.id].direction = "reverse";
    }
    indivMotor.motor[this.id].speed = Math.abs(parseInt($("#" + this.id).val() ));
});

/* Change runTime variable here if modem drivers are overloaded */
var runTime = 100;   

var entity = "navigator";
var motor_control  = undefined;
var registered = false;
var socket = undefined;
var inputType = undefined;
var ip = undefined;

var obj = {
    smartController: "OFF",
    motorDebug: "OFF",
    signaltype: "auto", //man, smart, auto 
    speed: 0, //  +/- 360DEG
    angle: 90, //  0 ~ 100
};
var prev_sig = {
    angle: 90,
    speed: 0
};

var indivMotor = {
    motor: {
        m1: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        },
        m2: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        },
        m3: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        },
        m4: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        },
        m5: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        },
        m6: {
            state: "OFF",
            direction: 'auto',
            speed: 'auto'
        }
    }
}
var prev_indivMotor = {
    m1: {
        direction: 'auto',
        speed: 'auto'
    },
    m2: {
        direction: 'auto',
        speed: 'auto'
    },
    m3: {
        direction: 'auto',
        speed: 'auto'
    },
    m4: {
        direction: 'auto',
        speed: 'auto'
    },
    m5: {
        direction: 'auto',
        speed: 'auto'
    },
    m6: {
        direction: 'auto',
        speed: 'auto'
    }
}

var key = []; //  keyCode events
var maxSpeed;
var pollRoverMovement;
var halt = false;
var gamepad_indivMotor_pressed = false;


var x = 0, //  tan2(y,x) assume forward default
    y = 0;
var randomNum = 0; //  For speed calculation

var pollChromeGamepad; 
var padLoop = false;
var controllers = {}; // Connected controllers 
var gamepadEvent = 'ongamepadconnected' in window;
var gp; // controllers[0] rover controller

$('#connect').on('click', function() {
    if (registered) { return; }
    ip = $("#server").find(":selected").val();
    feedback.log("Connecting to: " + ip);
    if (socket == undefined) {
        socket = io('http://' + ip + ':8085');
    } else {
        socket = undefined;
        socket = io('http://' + ip + ':8085', {
            'forceNew': true //Required to reconnect
        });
    }
    if (socket == undefined) {
        feedback.log("No socket connection!");
        return;
    } else {
        inputType = $("#control").find(":selected").val();
        socket.on('connect', function() {
            feedback.log('SERVER IS AVAILABLE');
            $("#server_status").css("background", "lime");
            // ======== Emit Registration ======== //
            socket.emit("REGISTER", {
                entity: entity,
                password: 'destroymit'
            });
            // =========== SERVER SIGNAL =========== //
            socket.on('SERVERSIG', handleServerSig );
            // =========== ROVER SIGNAL =========== //
            socket.on('ROVERSIG', handleRoverSignal);
            // =========== DISCONNECT SIGNAL =========== //
            socket.on('disconnect', function() {
                feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
                $("#core_status").css("background", "grey");
                $("#oculus_status").css("background", "grey");
                $("#captain_status").css("background", "grey");
                $("#tracker_status").css("background", "grey");
                $("#archaeologist_status").css("background", "grey");
                $("#navigator_status").css("background", "grey");
                $("#"+entity+"_status").css("background", "red");
                $("#server_status").css("background", "red");

                if (inputType == 'keyboard') {
                    $(document).off('keydown keyup');
                } else {
                    gp = undefined;
                    clearInterval(pollRoverMovement);
                }

                $('.motor > button').removeClass('ON').addClass('OFF').off('click');
                $("#m1, #m2, #m3, #m4, #m5, #m6").slider('setValue', 0); 

                registered = false;
            });
        });
    }
});

$("#disconnect").on('click', function() {
    if (socket != undefined) {
        /*console.log('Disconnect triggered!');
        socket.disconnect();
        registered = false;*/

        location.reload();
    }
});

function handleRoverSignal(data) {
    var directive = data["directive"];
    var info = data["info"];
    switch(directive) {
        case "SENSORS":
            degrees = info["compass"]["heading"];
            draw();

            gyro = {
                'x': Math.round(info["accelero"]["x"] * 100) / 100, 
                'y': Math.round(info["accelero"]["y"] * 100) / 100, 
                'z': Math.round(info["accelero"]["z"] * 100) / 100 
            };
            $("#gyro").text(gyro["x"] + ' ' + gyro["y"] + ' ' + gyro["z"]);

            accel = {
                'x': Math.round(info["gyro"]["x"] * 100) / 100, 
                'y': Math.round(info["gyro"]["y"] * 100) / 100, 
                'z': Math.round(info["gyro"]["z"] * 100) / 100 
            };
            $("#accel").text(accel["x"] + ' ' + accel["y"] + ' ' + accel["z"]);
            break;
        default:
            feedback.log("Rover " + info);
            break;
    }   
}

function handleDisconnection(who) {
    if (who == entity) {
        $("#"+entity+"_status").css("background", "red");
        registered = false;
    } else if (who == "cortex") {
        $("#"+who+"_status").css("background", "red");
        feedback.log("Rover disconnected from server!");

        if (inputType == 'keyboard') {
            $(document).off('keydown keyup');
        } else {
            gp = undefined;
            clearInterval(pollRoverMovement);
            feedback.log('Gamepad deactivated!');
        }
        $('.motor > button').removeClass('ON').addClass('OFF').off('click');

    } else {
        $("#"+who+"Con").css('color', 'red');
    }
}

function handleConnection(who) {
    if (who == entity) {
        $("#"+entity+"_status").css("background", "lime");
        registered = true;
    } else if(who == "cortex") {
        feedback.log("Rover connected to server!");
        $("#"+who+"_status").css("background", "lime");

        if (inputType == 'keyboard') {
            bindKeyboard();
        } else {
            bindJoystick();
        }

        motorControl();
    } else {
        $("#"+who+"_status").css("background", "lime");
        feedback.log(who+" CONNECTED FROM SERVER");
    }
}

function handleServerSig(data) {
    feedback.log("INCOMING SERVERSIG", data);
    var directive = data["directive"];
    var info = data["info"];
    switch(directive) {
        case "PASSWORD_INCORRECT":
            $("#"+entity+"_status").css("background", "red");
            break;  
        case "CONNECT":
            handleConnection(info);
            break;  
        case "CONNECTIONS":
            handleConnections(info);
            break;
        case "SENSORS":
            handleSensors(info);
            break;
        case "DISCONNECT":
            handleDisconnection(info);
            break;
        default:
            feedback.log("Server " + info);
            break;
    }
}

function bindKeyboard() {
    feedback.log("Bind motors triggered");
    $(document).on('keydown keyup', function(e) {
        key[e.keyCode] = e.type == 'keydown';
        var fullReset = setTimeout(function() {
            x = 0, y = 0;
            obj["speed"] = 0;
            randomNum = 0;
            obj["angle"] = 90;
            sendCTRLSIG();
        }, 50);

        y = 0, x = 0;
        if (key[87]) { // 'W' KEY //
            y += 1;
            clearInterval(fullReset);
        }
        if (key[83]) { // 'S' KEY //
            y += -1;
            clearInterval(fullReset);
        }
        if (key[65]) { // 'A' KEY //
            x += -1;
            clearInterval(fullReset);
        }
        if (key[37]) { // 'LEFT' KEY //
            x += -1;
            clearInterval(fullReset);
        }
        if (key[68]) { // 'D' KEY //
            x += 1;
            clearInterval(fullReset);
        }
        if (key[39]) { // 'RIGHT' KEY //
            x += 1;
            clearInterval(fullReset);
        }
        if (key[32]) { // 'SPACE' KEY //   
            obj["speed"] = 0;
            clearInterval(fullReset);
            sendCTRLSIG();
        }
        /* Allow speed change ONLY if 'W' or 'S' key are pressed */
        if (key[87] || key[83]) {
            if (key[38]) { // 'UP' KEY //
                if (obj["speed"] < 100) {
                    randomNum += 5;
                    obj["speed"] = Math.round(Math.min(randomNum, 100));
                    sendCTRLSIG();
                }
            }
            if (key[40]) { // 'DOWN' KEY //
                if (obj["speed"] > 0) {
                    randomNum -= 5;
                    obj["speed"] = Math.round(Math.max(randomNum, 0));
                    sendCTRLSIG();
                }
            }
        }
        /* Compute rover direction using function atan(y,x). Unlike dot product (scalar),function takes 
        into account unit circle quadrant thus allowing for negative degrees - rounding error +/- half degree */
        if ((x != 0) || (y != 0)) {
            computeDirection();
        }
    });
}

function gamepadChromeHandler() {
    var is_chrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1;
    if (is_chrome) {
        pollChromeGamepad = setInterval(function() {
            gp = navigator.getGamepads()[0];
        }, 100);
    }
}

function verifyGamepad() {
    if (!controllers) {
        return;
    } else {
        if (gp == undefined) {
            gp = controllers[0];
        }
    }
}

function handleRotate() { 
    if (gp["buttons"][2]["value"] == 1) {    //  Left-lower button
        obj["angle"] = 362;
        obj["speed"] = maxSpeed;
        halt = true;
        sendCTRLSIG();
    } else if (gp["buttons"][3]["value"] == 1) { // Right-lower Button 
        obj["angle"] = 361;
        obj["speed"] = maxSpeed;
        halt = true;
        sendCTRLSIG();
    }
}

function computeMagnitude() {
    obj["speed"] = Math.sqrt((x*x) + (y*y));
    if(obj["speed"] >= 1.0) {
        obj["speed"] = 1;
    }
    obj["speed"] = Math.round(obj["speed"] * maxSpeed);
}

function computeDirection() {
    obj["angle"] = Math.atan2(y, x);
    obj["angle"] = Math.round((obj["angle"] < 0 ? 2 * Math.PI + obj["angle"] : obj["angle"]) * 180 / Math.PI);
    if(obj["angle"] > 88 && obj["angle"] < 92) {
        obj["angle"] = 90;
    }
}

function turnOnIndivMotor(motor) {
    var direction; 
    if(obj["angle"] >= 0 && obj["angle"] <= 180) {
        direction = "forward";
    } else {
        direction = "reverse";
    }

    indivMotor["motor"][motor]["speed"] = obj["speed"];
    indivMotor["motor"][motor]["state"] = 'ON';
    indivMotor["motor"][motor]["direction"] = direction;

    if ( (prev_indivMotor[motor]["speed"] != indivMotor["motor"][motor]["speed"]) 
        || (prev_indivMotor[motor]["direction"] != indivMotor["motor"][motor]["direction"]) ) {

        gamepad_indivMotor_pressed = true;
        prev_indivMotor[motor]["speed"] = indivMotor["motor"][motor]["speed"];
        prev_indivMotor[motor]["direction"] = indivMotor["motor"][motor]["direction"];

        sendCTRLSIG();
    }
}

function turnOffIndivMotor(motor) {
    if(indivMotor["motor"][motor]["state"] == "ON") {
        indivMotor["motor"][motor]["speed"] = 0;
        sendCTRLSIG();
    }
    indivMotor["motor"][motor]["speed"] = "auto";
    indivMotor["motor"][motor]["state"] = "OFF";
    indivMotor["motor"][motor]["direction"] = "auto"

    prev_indivMotor[motor]["speed"] = indivMotor["motor"][motor]["speed"];
    prev_indivMotor[motor]["direction"] = indivMotor["motor"][motor]["direction"];
}


function handleIndivMotor() {
    if(gp["buttons"][6]['pressed'] && $('button[value="m1"]').hasClass('OFF')) {
        turnOnIndivMotor("m1");

    } else if(!gp["buttons"][6]['pressed'] && $('button[value="m1"]').hasClass('OFF')) {
        turnOffIndivMotor("m1");  
    }
    if(gp["buttons"][7]['pressed'] && $('button[value="m2"]').hasClass('OFF')) {
        turnOnIndivMotor("m4");

    } else if(!gp["buttons"][7]['pressed'] && $('button[value="m2"]').hasClass('OFF')) {
        turnOffIndivMotor("m4");
    } 
    if(gp["buttons"][8]['pressed'] && $('button[value="m3"]').hasClass('OFF')) {
        turnOnIndivMotor("m2");

    } else if(!gp["buttons"][8]['pressed'] && $('button[value="m3"]').hasClass('OFF')) {
        turnOffIndivMotor("m2");
    }
    if(gp["buttons"][9]['pressed'] && $('button[value="m4"]').hasClass('OFF')) {
        turnOnIndivMotor("m5");

    } else if(!gp["buttons"][9]['pressed'] && $('button[value="m4"]').hasClass('OFF')) {
      turnOffIndivMotor("m5");
    }
    if(gp["buttons"][10]['pressed'] && $('button[value="m5"]').hasClass('OFF')) {
        turnOnIndivMotor("m3");

    } else if(!gp["buttons"][10]['pressed'] && $('button[value="m5"]').hasClass('OFF')) {
        turnOffIndivMotor("m3"); 
    }
    if(gp["buttons"][11]['pressed'] && $('button[value="m6"]').hasClass('OFF')) {
        turnOnIndivMotor("m6");

    } else if(!gp["buttons"][11]['pressed'] && $('button[value="m6"]').hasClass('OFF')) {
        turnOffIndivMotor("m6");
    }

    if ( gamepad_indivMotor_pressed && (!gp["buttons"][6]['pressed'] && !gp["buttons"][7]['pressed'] && !gp["buttons"][8]['pressed'] && 
        !gp["buttons"][9]['pressed'] && !gp["buttons"][10]['pressed'] && !gp["buttons"][11]['pressed'])) {

        for(var i = 1; i <= 6; i++) {
            indivMotor["motor"]["m" + i.toString()]["speed"] = 0;
            indivMotor["motor"]["m" + i.toString()]["direction"] = 90;
        }
        sendCTRLSIG(); 
        gamepad_indivMotor_pressed = false;
    }
}

function handleRoverMovement() {
    gamepadChromeHandler();
    verifyGamepad();
    pollRoverMovement = setInterval(function() {
        if (gp == undefined) {
            clearInterval(pollRoverMovement);
            return;
        }

        y = 0, x = 0;

        if (gp["axes"][3] != 10000000000) { 
            // NOTE: axes[3] will lock to 0 if near 0 (also shown as half way between + and -)
            // Speed 50 === gp.axes[3] when 0 
            //obj.speed = Math.round((gp.axes[3] - 1) * -50);
            maxSpeed = Math.round((gp.axes[3] - 1) * -50);
        }

        handleRotate();

        if(!gp["buttons"][2]["pressed"] && !gp["buttons"][3]["pressed"] ) {
            if (gp["axes"][0] > 0.1 || gp["axes"][0] < -0.1) {
                x = gp["axes"][0];
            }
            if (gp["axes"][1] > 0.1 || gp["axes"][1] < -0.1) {
                y = -1 * gp["axes"][1];
            }
            if ((gp["axes"][0] < 0.15 && gp["axes"][0] > -0.15) && (gp["axes"][1] < 0.15 && gp["axes"][1] > -0.15)) {
                obj["angle"] = 90;
                obj["speed"] = 0;
                
                if(halt) {
                    sendCTRLSIG();
                    halt = false;
                }

            } else /*if ((x != 0) || (y != 0))*/ {
                computeDirection();
                computeMagnitude();

                halt = true;
                if(gp["buttons"][0]["pressed"]) { // Reverse trigger - forward case
                    if ( (obj["angle"] > 45) && (obj["angle"] < 135) ) {
                        obj["angle"] = 90;
                        obj["speed"] = 0;  
                    } else if ( (obj["angle"] >= 135) && (obj["angle"] <= 180) ) {
                        obj["angle"] = 180;
                    } else if ( (obj["angle"] >= 0) && (obj["angle"] <= 45) ) {
                        obj["angle"] = 0
                    }
                } else {    // Reverse trigger - reverse case
                    if ( (obj["angle"] > 225) && (obj["angle"] < 315) ) {
                        obj["angle"] = 90;
                        obj["speed"] = 0;  
                    } else if ( (obj["angle"] >= 180) && (obj["angle"] <= 225) ) {
                        obj["angle"] = 180;
                    } else if ( (obj["angle"] >= 315) && (obj["angle"] <= 360) ) {
                        obj["angle"] = 0
                    }
                }
                sendCTRLSIG();
            }
            handleIndivMotor();
        }
    }, runTime);
}

function gamepadHandler(event, connecting) {
    var gamepad = event.gamepad; 
    // NOTE: gamepad === navigator.getGamepads()[gamepad.index]
    if (connecting) {
        controllers[gamepad.index] = gamepad;
    } else {
        delete controllers[gamepad.index];
    }
}

function bindJoystick() {
    if(gp != undefined) {   // Clear polling required for server reconnect
        clearInterval(padLoop);
    }
    $(window).on('gamepadconnected', function(e) {
        e = e.originalEvent;
        clearInterval(x);
        feedback.log("Gamepad connected at index " +
            e.gamepad.index + ": " + e.gamepad.id + ". " +
            e.gamepad.buttons.length + " buttons, " + e.gamepad.axes.length + " axes.");
        gamepadHandler(e, true);

        handleRoverMovement();
    });

    $(window).on("gamepaddisconnected", function(e) {
        e = e.originalEvent;
        feedback.log("Gamepad disconnected from index " + e.gamepad.index + ":" + e.gamepad.id);
        gamepadHandler(e, false);
    });

    /* 
    *  Chrome compatibility - poll for gamepad connection if connected already 
    *  NOTE: Firefox saves/updates current state of gamepad 
    *        Chrome does not - requires constant polling for state
    */       
    if (!gamepadEvent) {
        padLoop = setInterval(function() {
            feedback.log('Checking for gamepad!');
            var pads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
            for (var i = 0; i < pads.length; i++) {
                if (pads[i]) {
                    controllers[pads[i].index] = pads[i];
                    clearInterval(padLoop);
                    handleRoverMovement();

                    feedback.log("Gamepad connected at index " +
                        pads[i].index + ": " + pads[i].id + ". " +
                        pads[i].buttons.length + " buttons, " + pads[i].axes.length + " axes.");

                }
            }
        }, 500);
    }
}

function sendCTRLSIG() {
    $(".motor_control").each(function() {
        if($(this).prop('checked') ) {
            motor_control = $(this).attr('data-label-text');
            //feedback.log("Motor control set to " + motor_control);
        }
    });
    
    if(motor_control == undefined) return;

    switch(motor_control) {
        case "manual": 
            if( ($('.motor > button.ON').length > 0) || gamepad_indivMotor_pressed) {
                var ext = {};
                $.extend(ext, obj, indivMotor);
                ext.signaltype = "man";

                socket.emit("CTRLSIG", {
                    directive: 'MOTOR',
                    info: ext
                });

                feedback.log("man signal sent");
            } else { 
                /* If accidently set to manual, stop movement */
                obj["signaltype"] = "auto";
                obj["speed"] = 0;
                obj["angle"] = 90;
 
                socket.emit("CTRLSIG", {
                    directive: 'MOTOR',
                    info: obj
                });

                prev_sig["angle"] = obj["angle"];
                prev_sig["speed"] = obj["angle"];

                feedback.log("Warning: Attempted manual control!");
            }
            break;
        case "smart":
            if ((prev_sig["angle"] != obj["angle"]) || (prev_sig["speed"] != obj["speed"]) || gamepad_indivMotor_pressed || ($('.motor > button.ON').length > 0)) {
                var ext = {};
                $.extend(ext, obj, indivMotor);   
                ext.signaltype = "smart";
                socket.emit("CTRLSIG", {
                    directive: 'MOTOR',
                    info: ext
                });

                prev_sig["angle"] = obj["angle"];
                prev_sig["speed"] = obj["angle"];

                feedback.log(motor_control + " sent");
            } 
            break;
        case "auto":
            if ((prev_sig["angle"] != obj["angle"]) || (prev_sig["speed"] != obj["speed"])) {
                obj.signaltype = "auto";
                socket.emit("CTRLSIG", {
                    directive: 'MOTOR',
                    info: obj
                });

                prev_sig["angle"] = obj["angle"];
                prev_sig["speed"] = obj["angle"];

                feedback.log(motor_control + " sent");
            } 
            break;
        default:
            feedback.log("Unknown motor command.");
            break;
    }
}

function motorControl() {
    $('.motor > button').on('click', function(event) {
        if(gamepad_indivMotor_pressed) return;

        var motorVal = $(this).val(); 
        if ($(this).hasClass('OFF')) {
            $(this).removeClass('OFF').addClass('ON'); // ON state

            /* Animate scroll to current speed */
            if ((obj["angle"] >= 0) && (obj["angle"] <= 180)) {
                $("#" + motorVal).slider('setValue', obj["speed"]);
                indivMotor["motor"][motorVal]["direction"] = "forward";
                prev_indivMotor[motorVal]["direction"] = "forward";
            } else {
                $("#" + motorVal).slider('setValue', -1 * obj["speed"]);
                indivMotor["motor"][motorVal]["direction"] = "reverse";
                prev_indivMotor[motorVal]["direction"] = "reverse";
            }
            /* Emit with current speed */
            indivMotor["motor"][motorVal]["state"] = 'ON';
            indivMotor["motor"][motorVal]["speed"] = obj["speed"];
            prev_indivMotor[motorVal]["speed"] = indivMotor["motor"][motorVal]["speed"];
            sendCTRLSIG();

            $("#" + motorVal).on('slideStop', function() {
                prev_indivMotor[motorVal]["direction"] = indivMotor["motor"][motorVal]["direction"];
                prev_indivMotor[motorVal]["speed"] = indivMotor["motor"][motorVal]["speed"];    
                sendCTRLSIG();
            });

        } else /*if ($(this).hasClass('ON'))*/ {
            indivMotor["motor"][motorVal]["speed"] = 0;
            $("#" + motorVal).slider('setValue', 0);
            $("#" + motorVal).off('slideStop');
            sendCTRLSIG();

            $(this).removeClass('ON').addClass('OFF');

            /* Emitt off individual motor */
            indivMotor["motor"][motorVal]["state"] = 'OFF';
            indivMotor["motor"][motorVal]["direction"] = "auto";
            indivMotor["motor"][motorVal]["speed"] = 'auto';

            prev_indivMotor[motorVal]["direction"] = "auto";
            prev_indivMotor[motorVal]["speed"] = "auto";

            socket.emit("CTRLSIG", {
                directive: 'MOTOR',
                info: indivMotor
            });
        }

    });
}

$(document).on('keydown', function(event) {
    if (event.keyCode == 8) {
        var target = event.target.tagName;
        if (target.toLowerCase() === 'input' ||
            target.toLowerCase() === 'textarea') {
            return;
        } else {
            event.preventDefault();
        }
    }
}).on('keydown keyup', function(event) {
    if(event.keyCode ==  49) {
        $("#m1").click();
    }
    if(event.keyCode == 50) {
        $("#m2").click();
    }
    if(event.keyCode == 51) {
        $("#m3").click();
    }
    if(event.keyCode == 52) {
        $("#m4").click();
    }
    if(event.keyCode == 53) {
        $("#m5").click();
    }
    if(event.keyCode == 54) {
        $("#m6").click();
    }
});

$("#debug").on('click', function() {
    if($(this).hasClass('btn-danger')) {
        $(this).removeClass('btn-danger').addClass('btn-success');
        obj["motorDebug"] = "ON";
    } else {
        $(this).removeClass('btn-success').addClass('btn-danger');
        obj["motorDebug"] = "OFF";
    }
});
