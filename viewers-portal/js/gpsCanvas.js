/**
*gpsCanvas.js manages the minimap of the Johnson Space Center. This script manages
*the blinking lights on the minimap.
**/



//GPS calculations
var centerX;  //Center of the canvas element
var centerY;
var roverLength;
var roverRot = 0; //Rover's roation. Relative to East going counter-clockwise
var center;   	//Rover's GPS coordinates   -- right now it is static. Will need to change once the GPS module is up and running
var coords;		//List of coordinates of interest
var height;		//Height of camera mast (ft), used in trig calculations
var trueYaw;

var roverLat;
var roverLong;

function init() {
	roverLat = 29.564887;
	roverLong = -95.081316;
	
	//Initializing the variables above
	center = [roverLat, roverLong];
	height = 3;
	
}

/*
Calculates the coordinates of a point of interest given the pitch and yaw of the camera mast.
Right now, the function does not take into account the rover's dynamic orientation. Again,
this will be updated when the GPS module is up and running.

@param 	Angle	Value of the rover's current rotation relative to North going clockwise
@param 	Yaw 	Value of the yaw   of the camera mast
@return void    
*/
function calculateGPS(angle, yaw, distance) {

	console.log("Calculating GPS coordinate at Yaw: " + yaw + "    Rover Rotation: " + angle  +   "     Lidar Reading: " + distance);
	//var dist = height * Math.tan(pitch * Math.PI / 180);    //Old calculation
	var liderReading = distance * 3.28;   //meters to ft
	var dist = Math.sqrt(Math.pow(liderReading,2) - Math.pow(height, 2));
	var theta = 180 - (angle + 90) % 360;
	roverRot = theta < 0 ? theta + 360 : theta;

	trueYaw = (yaw - 90) + roverRot;
	console.log(dist);
	var dLat = dist/(370800) * Math.sin(toRads(trueYaw));  //Using ft.
	var dLong = dist/(370800) * Math.cos(toRads(trueYaw));

	//Here is where we will need to take into account the rover's orientation
	var point = [center[0] + dLat, center[1] + dLong];
	addMarker(point);
	console.log("Coordinate point: " + point[0] + " " + point[1]);
	
}




