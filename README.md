**Mar 14 2015**

* Video directives sends a OCULARSIG and not a CTRLSIG. This is due to the seperation of Oculus (video feed handler) and RoverCore (Everything else).
* CTRLSIG must be coupled with both a directive and information. No directive only messages.
* One can now see all of the users who have been logged in currently.
* There is now a section for the sensors information, sent from the server.
* The disconnect button merely reloads the webpage. This is literally the cleanist way to do it... I am serious about that... I mean really, sometimes Socket.io can be a real bitch!
* More options for the server you can select have been added. kammce.io, sce.engr.sjsu.edu are new entries.
* Code has been slightly clean up... maybe, I am not totally sure. I like it though.

**Jan 8 2015**

* Complete overhaul of the mission control testing page with a JSON ide interface to send JSON messages to the server.