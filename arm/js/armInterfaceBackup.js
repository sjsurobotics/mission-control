var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function(err) {
		alert(err.toString());
	}
};
function CTRLPACKET() { //prototype of packet to send to server then to Arm.handle()
	// this.base = 0;
	// this.shoulderL = 0;
	// this.shoulderR = 0;
	// this.elbow = 0;
	// this.wrist = 0;
	this.speed = 50;
};
var cnt = 0;
var prevpos = 0;
var mimicComm = false;
var prevID = '';
var mimicPositions = {
	base: 0,
	shoulderL: 0,
	shoulderR: 0,
	elbow: 0,
	wrist: 0
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		shoulderL: 0,
		shoulderR: 0,
		elbow: 0,
		wrist: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res: 400,
		width: 640,
		height: 480,
		fps: 20
	}
};
// var editor = new JSONEditor(document.querySelector("#info"), options, schemas["MOTOR"]);
// var sensor = new JSONEditor(document.querySelector("#sensor"), options);
var feedback = {
	log: function(str, obj) {
		if (typeof obj == "undefined") {
			console.log(str);
			$("#log").prepend(str + "\n");
		} else {
			console.log(str, obj);
			$("#log").prepend(str + " " + JSON.stringify(obj) + "\n");
		}
		// keep the log on the bottom
		//var textarea = document.getElementById('log');
		//textarea.scrollTop = textarea.scrollHeight;
	}
};
var entity;
var socket = undefined;
var registered = false;
var getSensors = undefined;
//// VIDEO FEED
// var mcanvas = document.getElementById('multi-cam');
// var mctx = mcanvas.getContext('2d');

// mctx.fillStyle = '#CCC';
// mctx.fillText('... WAITING FOR SERVER ...', mcanvas.width / 2 - 60, mcanvas.height / 2);

// Setup the WebSocket connection and start the player
//var mclient = new WebSocket( 'ws://discovery.srkarra.com:9000/' );
// var mclient, mplayer;
//// DOM Event listeners
motorCtrl();
$("#connect").on("click", function() {
	console.log("Clicked");
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
		// mclient = new WebSocket('ws://' + address + ':9000/');
		// mplayer = new jsmpeg(mclient, {
		// 	canvas: mcanvas
		// });
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', function(data) {
		feedback.log("INCOMING OCULARSIG", data);
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#core_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		$('.scrollbar').off('scroll');
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", {
		entity: entity,
		password: "destroymit"
	});
});
$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		// //socket.disconnect();
		// socket.io.close();
		// socket = null;
		// socket = undefined;
		// registered = false;
		location.reload(); //refresh page
	}
});
// $("#ctrl").on("click", function() {
// 	if (!registered) { return; }
// 	var directive = $("#directive").val();
// 	var info = editor.get();
// 	if (info == "") {
// 		bootbox.alert("You must send information with your directive.");
// 	} else {
// 		if(directive == "VIDEO") {
// 			socket.emit("OCULARSIG", {
// 				directive: directive,
// 				info: info
// 			});
// 		} else {
// 			socket.emit("CTRLSIG", {
// 				directive: directive,
// 				info: info
// 			});
// 		}
// 	}
// });



// $("#gpsCtrl").on("click", function() {
// 	if(!registered) return;
// 	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Get Rotations"}});
// });

// $(".control-panel").on("click", function() {
// 	if(!registered) return;
// 	var data = getAngles();
// 	console.log("Sending angle directives");
// 	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Rotations", pitch: data.pitch, yaw: data.yaw}});
// });

// $("#directive").on("change", function() {
// 	editor.set(schemas[$(this).val()]);
// });

$(document).unbind('keydown').bind('keydown', function(event) {
	var doPrevent = false;
	if (event.keyCode === 8) {
		var d = event.srcElement || event.target;
		if ((d.tagName.toUpperCase() === 'INPUT' &&
				(
					d.type.toUpperCase() === 'TEXT' ||
					d.type.toUpperCase() === 'PASSWORD' ||
					d.type.toUpperCase() === 'FILE' ||
					d.type.toUpperCase() === 'EMAIL' ||
					d.type.toUpperCase() === 'SEARCH' ||
					d.type.toUpperCase() === 'DATE')
			) ||
			d.tagName.toUpperCase() === 'TEXTAREA') {
			doPrevent = d.readOnly || d.disabled;
		} else {
			doPrevent = true;
		}
	}
	if (doPrevent) {
		event.preventDefault();
	}
});

// ====================================
function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

function motorCtrl(){
	//Input Toggle: Switches input Type
	$('#controltype').on('click', function(event){
		if($(this).val() == "Scrollbar"){
			$(this).val("Text Entry"); //switch control type
			document.getElementById('controltype').innerHTML = "Text Entry";
		}
		else if($(this).val() == "Text Entry"){
			$(this).val("Mimic"); //switch control type
			document.getElementById('controltype').innerHTML = "Mimic";
		}
		else if($(this).val() == "Mimic"){
			$(this).val("Scrollbar"); //switch control type
			document.getElementById('controltype').innerHTML = "Scrollbar";	
		}
	});
	//Motor Controls
	$('.motor > button').on('click', function(event){ //JQuery: for all elements of class 'motor' and type 'button' (i.e. motor control buttons)...
		var motorID = $(this).val().toString();
		if($(this).hasClass("button-inactive")){ //is inactive at first
			document.getElementById(motorID+"-status").innerHTML = "Interface Engaged";
			$("#" + motorID + "-status").css("color", "lime");
			$(this).removeClass("button-inactive").addClass("button-active"); //turns button on
			$('scrl-' + $(this).val().toString()).prop("overflow-y", "scroll"); //activates scrollbar
			console.log("motorID: " + motorID);
			console.log(CTRLPACKET);
			// Action for submission of degree textbox form element:
			if($('#controltype').val() == 'Text Entry'){
				console.log("Mode: Text Entry");
				if(mimicComm = true){
					clearInterval(mimicServer);
					mimicComm = false;
				}
				$('#txtbutton' + motorID).on('click', function(event){
					var pos = $('#txt' + motorID).val().toString(); //Take in degree value
					//Handle Degree Overflow
					if(pos > 360){
						pos = 360;
						$('#txt' + motorID).val("360");
					}
					else if(pos < 0){
						pos = 0;
						$('#txt' + motorID).val("0");
					}
					//Put packet together
					var ctrlpacket = new CTRLPACKET();
					if(motorID == 'base'){
						ctrlpacket.base = pos;
					}
					else if(motorID == 'shlds'){//shoulder diabled until offset can be accounted for
						ctrlpacket.shoulderL = pos;
					}
					else if(motorID == 'elbow'){
						ctrlpacket.elbow = pos;
					}
					else if(motorID == 'wrist'){
						ctrlpacket.wrist = pos;
					}
					//Send packet if degree information is not redundant
					if(prevpos != pos || prevID != motorID){
						socket.emit("CTRLSIG", {directive:"ARM", info: ctrlpacket});
						prevpos = pos;
						prevID = motorID;
					}
					document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
					$('#scrl-' + motorID).scrollTop(pos);
				});
			}
			// Action for scrollbar control:
			else if($('#controltype').val() == 'Scrollbar'){
				console.log("Mode: Scrollbar");
				if(mimicComm = true){
					clearInterval(mimicServer);
					mimicComm = false;
				}
				$('#scrl-' + motorID).scroll(function(){
					var pos = $('#scrl-' + motorID).scrollTop().toFixed();
					var ctrlpacket = new CTRLPACKET(); //In JS as opposed to C/C++, using "new" without "delete" won't cause memory leak. Once function ends, the used memory space is dealt with by JS's garbage collector, putting back the used memory space into the freestore (heap)
					console.log(motorID + ': ' + pos);
					document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
					if(motorID == 'base'){
						ctrlpacket.base = pos;
					}
					else if(motorID == 'shlds'){//shoulder diabled until offset can be accounted for
						ctrlpacket.shoulderL = pos;
					}
					else if(motorID == 'elbow'){
						ctrlpacket.elbow = pos;
					}
					else if(motorID == 'wrist'){
						ctrlpacket.wrist = pos;
					}
					if(pos != prevpos){
						socket.emit("CTRLSIG", {directive: "ARM", info: ctrlpacket}); //send sig to server
						prevpos = pos;
					}
					/*Having CTRLPACKET as an object allows me to customize the size of data to be sent over to rover.
					For example: Suppose that I am scrolling for base motor only. A new object "ctrlpacket" is made,
					with only speed at first. Depending on which bar I am controlling, the if statements below will
					only add the attribute of the current motor being manipulated, so when the packet is sent, it will
					not include unnecessaryly large amounts of data. Scrolling base will only give out the packet with
					speed and base value only. Arm.js checks for which elements are present in packet, and since base
					and speed are all that's there, arm.js will only send out two control packets, one controlling this
					motor, and the other editting speed*/
				});
			}
			// Action for Mimic control option
			else if($('#controltype').val() == "Mimic"){
				console.log("Mode: Mimic");
				mimicComm = true;
				var mimicServer = setInterval(function(){
					$.get("127.0.0.1:5000", function(data, status){
						var ctrlpacket = new CTRLPACKET();
						if(typeof data.base != undefined){
							if(data.base != mimicPositions.base){
								ctrlpacket.base = data.base;
								mimicPositions.base = data.base;
							}
						}
						if(typeof data.shoulderL != undefined){
							if(data.shoulderL != mimicPositions.shoulderL){
								ctrlpacket.shoulderL = data.shoulderL;
								mimicPositions.shoulderL = data.shoulderL;
							}
						}
						if(typeof data.shoulderR != undefined){
							if(data.shoulderR != mimicPositions.shoulderR){
								ctrlpacket.shoulderR = data.shoulderR;
								mimicPositions.shoulderR = data.shoulderR;
							}
						}
						if(typeof data.elbow != undefined){
							if(data.elbow != mimicPositions.elbow){
								ctrlpacket.elbow = data.elbow;
								mimicPositions.elbow = data.elbow;	
							}
						}
						if(typeof data.wrist != undefined){
							if(data.wrist != mimicPositions.wrist){
								ctrlpacket.wrist = data.wrist;
								mimicPositions.wrist = data.wrist;
							}
						}
						socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});
					});
				},1000);
			}
		}
		else if($(this).hasClass("button-active")){ //is active at first
			// document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			$(this).removeClass("button-active").addClass("button-inactive"); //turns button off
			// $('#scrl-' + motorID).hidden = true;
			// $('#form-' + motorID).hidden = true;
			document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			$("#" + motorID + "-status").css("color", "red");
			$('scrl-' + $(this).val().toString()).prop("overflow-y", "hidden"); //deactivates scrollbar
			$('.scrollbar').off('scroll');
		}

	});
}

function handleRoverSignal(data) {
	//feedback.log("INCOMING ROVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			//sensor.set(info);
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}
}
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
});

