var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function(err) {
		alert(err.toString());
	}
};
function CTRLPACKET() { //prototype of packet to send to server then to Arm.handle()
	// this.base = 0;
	// this.shoulderL = 0;
	// this.shoulderR = 0;
	// this.elbow = 0;
	// this.wrist = 0;
	this.speed = 50;
};
var cnt = 0;
var initiallyPressed = false;
var prevpos = 0;
var mimicComm = false;
var mimicServer;
var prevID = '';
var mimicPositions = {
	base: 180,
	shoulder: 150,
	elbow: 150,
	wrist: 150
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		shoulderL: 0,
		shoulderR: 0,
		elbow: 0,
		wrist: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res: 400,
		width: 640,
		height: 480,
		fps: 20
	}
};
// var editor = new JSONEditor(document.querySelector("#info"), options, schemas["MOTOR"]);
// var sensor = new JSONEditor(document.querySelector("#sensor"), options);
var feedback = {
	log: function(str, obj) {
		if (typeof obj == "undefined") {
			console.log(str);
			$("#log").prepend(str + "\n");
		} else {
			console.log(str, obj);
			$("#log").prepend(str + " " + JSON.stringify(obj) + "\n");
		}
		// keep the log on the bottom
		//var textarea = document.getElementById('log');
		//textarea.scrollTop = textarea.scrollHeight;
	}
};
var entity;
var socket = undefined;
var registered = false;
var getSensors = undefined;

motorCtrl();
$("#connect").on("click", function() {
	console.log("Clicked");
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
		// mclient = new WebSocket('ws://' + address + ':9000/');
		// mplayer = new jsmpeg(mclient, {
		// 	canvas: mcanvas
		// });
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', function(data) {
		feedback.log("INCOMING OCULARSIG", data);
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#core_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		$('.scrollbar').off('scroll');
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", {
		entity: entity,
		password: "destroymit"
	});
});
$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		location.reload(); //refresh page
	}
});

$(document).unbind('keydown').bind('keydown', function(event) {
	var doPrevent = false;
	if (event.keyCode === 8) {
		var d = event.srcElement || event.target;
		if ((d.tagName.toUpperCase() === 'INPUT' &&
				(
					d.type.toUpperCase() === 'TEXT' ||
					d.type.toUpperCase() === 'PASSWORD' ||
					d.type.toUpperCase() === 'FILE' ||
					d.type.toUpperCase() === 'EMAIL' ||
					d.type.toUpperCase() === 'SEARCH' ||
					d.type.toUpperCase() === 'DATE')
			) ||
			d.tagName.toUpperCase() === 'TEXTAREA') {
			doPrevent = d.readOnly || d.disabled;
		} else {
			doPrevent = true;
		}
	}
	if (doPrevent) {
		event.preventDefault();
	}
});

// ====================================
function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

function motorCtrl(){
	feedback.log("Press the Control Type Button to activate Control!!!");
	//Input Toggle: Switches input Type
	$('#controltype').removeClass("btn-success").addClass("btn-danger");
	$('#controltype').css("background-color", "black");
	$('#controltype').on('click', function(event){
		if(!initiallyPressed){
			$(this).removeClass("btn-danger").addClass("btn-success");
			$('#controltype').css("background-color", "limegreen");
			$(this).val("Mimic");
			$('.txtbox').prop("diabled", true);
			initiallyPressed = true;
		}
		if($(this).val() == "Scrollbar"){
			$(this).val("Text Entry"); //switch control type
			document.getElementById('controltype').innerHTML = "Text Entry";
			$('.motor > button').off('click');
			$('.slide').slider('destroy');
			$('.slide').prop("hidden", true);
			$('.scrollbar').prop('disabled', true);
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			//Hide Text Inputs
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", false);
			$('.txtbutton').prop('hidden', false);
			TextControl();
		}
		else if($(this).val() == "Text Entry"){
			$(this).val("Mimic"); //switch control type
			document.getElementById('controltype').innerHTML = "Mimic";
			if(mimicComm == true){
				clearInterval(mimicServer);
				mimicComm = false;
			}
			$('.motor > button').off('click');
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			//Hide Text Inputs
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", true);
			$('.txtbutton').prop('hidden', true);
			$('.txtbutton').prop('disabled', true);
			$('.txtbutton').unbind();
			MimicControl();
		}
		else if($(this).val() == "Mimic"){
			$(this).val("Scrollbar"); //switch control type
			document.getElementById('controltype').innerHTML = "Scrollbar";
			if(mimicComm == true){
				clearInterval(mimicServer);
				mimicComm = false;
			}
			//Hide Text Inputs
			$('.slide').prop("hidden", false);
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", true);
			$('.txtbutton').prop('hidden', true);
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			ScrollbarControl();
		}
	});
}

function mimicHandler(data, status){ //ajax request sent by apache server, and server receives data from test.txt
	console.log("Request Successful...");
	var inputPacket = JSON.parse(data); //Parses test.txt's data to a JSON object
	// console.log("Data:" + data + " Status:" + status + " JSON:" + inputPacket);
	//var ctrlpacket = new CTRLPACKET();
	var valid_packet = false;
	var ctrlpacket = {
		shoulder: mimicPositions.shoulder,
		elbow: mimicPositions.elbow
	};
	if(inputPacket['base'] != mimicPositions.base){
		ctrlpacket.base = inputPacket["base"];
		mimicPositions.base = inputPacket["base"];
		// $('#scrl-base').scrollTop(inputPacket["base"]);
		$('#deg-base').html(inputPacket["base"].toString());
	} else {
		
	}
	if(typeof inputPacket["shoulder"] != "undefined"){
		if(inputPacket["shoulder"] /*> 45 && inputPacket["shoulder"] < 220*/ != mimicPositions.shoulder){ 
			var valid_packet = true;
			//basing pos on right shoulder now.
			// if(pos < 45){
			// 	pos = 45;
			// } else if(pos > 220){
			// 	pos = 220;
			// }
			// //ctrlpacket.shoulderR = pos;
			// ctrlpacket.shoulderL = -1*(pos - 300);
			//console.log("sL-" + ctrlpacket.shoulderL + " sR-" + ctrlpacket.shoulderR);
			mimicPositions.shoulder = inputPacket["shoulder"];
			ctrlpacket.shoulder = inputPacket["shoulder"];
			//mimicPositions.shoulderL = ctrlpacket.shoulderL;
		}
	} else {
		ctrlpacket.shoulder = mimicPositions.shoulder;
	}
	if(typeof inputPacket["elbow"] != "undefined"){ 
		if(inputPacket["elbow"] /*> 70 && inputPacket["elbow"] < 220*/ != mimicPositions.elbow){
			var pos = inputPacket["elbow"];
			if(pos < 70) { pos = 70; }
			else if(pos > 220) { pos = 220; }
			ctrlpacket.elbow = pos;
			mimicPositions.elbow = pos;
			// $('#scrl-elbow').scrollTop(pos);
			$('#deg-elbow').html(pos.toString());
		}
	}
	// if(typeof inputPacket["wrist"] != "undefined"){
	// 	if(inputPacket["wrist"] /*> 120 && inputPacket["wrist"] < 240*/ != mimicPositions.wrist){
	// 		var pos = inputPacket["wrist"];
	// 		if(pos < 120){
	// 					pos = 120;
	// 				}
	// 				else if(pos > 240){
	// 					pos = 240;
	// 				}
	// 		ctrlpacket.wrist = pos;
	// 		mimicPositions.wrist = pos;
	// 		// $('#scrl-wrist').scrollTop(pos);
	// 		$('#deg-wrist').html(pos.toString());
	// 	}
	// }
	if(valid_packet) {
		socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});	
	}
}

function MimicControl(){
/*Mimic Mode*/ //--> The master copy
	console.log("Mode: Mimic");
	mimicComm = true;

	mimicServer = setInterval(function() { //Text Request
		console.log("Inside setInterval");
		$.ajax({
			url: "http://127.0.0.1:8080/",
			dataType: 'text',
			cache: false,
			success: mimicHandler,
	        error: function(jqXHR, textStatus, errorThrown) {
	        	console.log("error!");
			}
		});
	}, 10);

	/*mimicServer = setInterval(function(){
		$.ajax({url: "127.0.0.1:5000", data: {directive: "ARM", info: "Hello"}, success: function(data){ //127.0.0.1:5000
			var ctrlpacket = new CTRLPACKET();
			if(typeof data['base'] != undefined){
				if(data['base'] != mimicPositions.base){
					ctrlpacket.base = data["base"];
					mimicPositions.base = data["base"];
				}
			}
			if(typeof data["shoulderL"] != undefined){
				if(data["shoulderL"] != mimicPositions.shoulderL){
					ctrlpacket.shoulderL = data["shoulderL"];
					mimicPositions.shoulderL = data["shoulderL"];
				}
			}
			if(typeof data["shoulderR"] != undefined){
				if(data["shoulderR"] != mimicPositions.shoulderR){
					ctrlpacket.shoulderR = data["shoulderR"];
					mimicPositions.shoulderR = data["shoulderR"];
				}
			}
			if(typeof data["elbow"] != undefined){
				if(data["elbow"] != mimicPositions.elbow){
					ctrlpacket.elbow = data["elbow"];
					mimicPositions.elbow = data["elbow"];	
				}
			}
			if(typeof data["wrist"] != undefined){
				if(data["wrist"] != mimicPositions.wrist){
					ctrlpacket.wrist = data["wrist"];
					mimicPositions.wrist = data["wrist"];
				}
			}
			socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});
		}});
		console.log("Request Sent...");
	},1000);*/
	
	/*mimicServer = setInterval(function(){ //Text Request
		$.ajax({url: "test.txt", success: function(data, status){ //ajax request sent by apache server, and server receives data from test.txt
			console.log("Request Successful...");
			var testinput = JSON.parse(data); //Parses test.txt's data to a JSON object
			// console.log("Data:" + data + " Status:" + status + " JSON:" + testinput);
			var ctrlpacket = new CTRLPACKET();
			if(typeof testinput['base'] != undefined){
				if(testinput['base'] != mimicPositions.base){
					ctrlpacket.base = testinput["base"];
					mimicPositions.base = testinput["base"];
					$('#scrl-base').scrollTop(testinput["base"]);
					$('#deg-base').html(testinput["base"].toString());
				}
			}
			if(typeof testinput["shoulderL"] != undefined){
				if(testinput["shoulderL"] != mimicPositions.shoulderL){
					ctrlpacket.shoulderL = testinput["shoulderL"];
					mimicPositions.shoulderL = testinput["shoulderL"];
					// $('#scrl-shlds').scrollTop(testinput["shoulderL"]);
					$('#deg-shlds').html(testinput["shoulderL"].toString());
				}
			}
			if(typeof testinput["shoulderR"] != undefined){
				if(testinput["shoulderR"] != mimicPositions.shoulderR){
					ctrlpacket.shoulderR = testinput["shoulderR"];
					mimicPositions.shoulderR = testinput["shoulderR"];
				}
			}
			if(typeof testinput["elbow"] != undefined){
				if(testinput["elbow"] != mimicPositions.elbow){
					ctrlpacket.elbow = testinput["elbow"];
					mimicPositions.elbow = testinput["elbow"];
					// $('#scrl-elbow').scrollTop(testinput["elbow"]);
					$('#deg-elbow').html(testinput["elbow"].toString());
				}
			}
			if(typeof testinput["wrist"] != undefined){
				if(testinput["wrist"] != mimicPositions.wrist){
					ctrlpacket.wrist = testinput["wrist"];
					mimicPositions.wrist = testinput["wrist"];
					// $('#scrl-wrist').scrollTop(testinput["wrist"]);
					$('#deg-wrist').html(testinput["wrist"].toString());
				}
			}
			socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});
		}});
	}, 1000);*/
}

function TextControl(){
	console.log("Mode: Text Entry");
	if(mimicComm == true) {
		clearInterval(mimicServer);
		mimicComm = false;
	}
	$('.motor > button').on('click', function(event){ //JQuery: for all elements of class 'motor' and type 'button' (i.e. motor control buttons)...
		var motorID = $(this).val().toString();
		if($(this).hasClass("button-inactive")){ //is inactive at first
			// document.getElementById(motorID+"-status").innerHTML = "Interface Engaged";
			// $("#" + motorID + "-status").css("color", "lime");
			$(this).removeClass("button-inactive").addClass("button-active"); //turns button on
			$('#txtbutton' + motorID).prop('disabled', false);
			// $('scrl-' + $(this).val().toString()).prop("overflow-y", "scroll"); //activates scrollbar
			console.log("motorID: " + motorID);
			console.log(CTRLPACKET);
/*Text Entry Mode*/
			$('#txt' + motorID).prop('disabled', false); //enable text box
			$('#form-' + motorID).keydown(function(event){
				if(event.keyCode == 13){
					event.preventDefault();
					console.log("prevented submission");
				}
			});
			$('#txtbutton' + motorID).on('click', function(event){
				var pos = Number($('#txt' + motorID).val().toString()); //Take in degree value
				//Handle Degree Overflow
				if(pos > 360){
					pos = 360;
					$('#txt' + motorID).val("360");
				}
				else if(pos < 0){
					pos = 0;
					$('#txt' + motorID).val("0");
				}
				//Put packet together
				var ctrlpacket = new CTRLPACKET();
				if(motorID == 'base' && pos != mimicPositions.base){
					ctrlpacket.base = pos;
					mimicPositions.base = pos;
				}
				else if(motorID == 'shlds'){ //Max shdl. R val is 220 dg for RX64
					if(pos < 45){
						pos = 45;
					}
					else if(pos > 220){
						pos = 220;
					}
					if(pos != mimicPositions.shoulderR){
						ctrlpacket.shoulderR = pos;
						ctrlpacket.shoulderL = -1*(pos - 300);
						console.log("sL-" + ctrlpacket.shoulderL + " sR-" + ctrlpacket.shoulderR);
						mimicPositions.shoulderR = pos;
						mimicPositions.shoulderL = ctrlpacket.shoulderL;
					}
				}
				else if(motorID == 'elbow' && pos != mimicPositions.elbow){
					if(pos < 70){
						pos = 70;
					}
					else if(pos > 220){
						pos = 220;
					}
					ctrlpacket.elbow = pos;
					mimicPositions.elbow = pos;
				}
				else if(motorID == 'wrist' && pos != mimicPositions.wrist){
					if(pos < 120){
						pos = 120;
					}
					else if(pos > 240){
						pos = 240;
					}
					ctrlpacket.wrist = pos;
					mimicPositions.wrist = pos;
				}
				//Send packet if degree information is not redundant
				// if(prevpos != pos || prevID != motorID){
					socket.emit("CTRLSIG", {directive:"ARM", info: ctrlpacket});
					prevpos = pos;
					prevID = motorID;
				// }
				document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
				// $('#scrl-' + motorID).scrollTop(pos);
			});
		}
		else if($(this).hasClass("button-active")){ //is active at first
			// document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			$('#txtbutton' + motorID).prop('disabled', true);
			$(this).removeClass("button-active").addClass("button-inactive"); //turns button off
			$('#scrl-' + motorID).hidden = true;
			$('#txt' + motorID).prop('disabled', true); //disable text box
			$('#txtbutton' + motorID).off('click');
			// document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			// $("#" + motorID + "-status").css("color", "red");
			// $('scrl-' + $(this).val().toString()).prop("overflow-y", "hidden"); //deactivates scrollbar
			$('.scrollbar').off('slide'); //for sliders
		}
	});
}
var delay = false;
function ScrollbarControl(){
	console.log("Mode: Scrollbar");
	if(mimicComm == true){
		clearInterval(mimicServer);
		mimicComm = false;
	}
	$('.motor > button').on('click', function(event){
		var motorID = $(this).val().toString();
		if($(this).hasClass("button-inactive")){ //is inactive at first
			// document.getElementById(motorID+"-status").innerHTML = "Interface Engaged";
			// $("#" + motorID + "-status").css("color", "lime");
			$(this).removeClass("button-inactive").addClass("button-active"); //turns button on
			$('scrl-' + $(this).val().toString()).prop("overflow-y", "scroll"); //activates scrollbar
			console.log("motorID: " + motorID);
			console.log(CTRLPACKET);
/*Scrollbar Mode*/
				$('#slide-' + motorID).slider({min:0, max:360, value:150, step: 5,
					formatter: function(value){
						if(delay) { return; }
						delay = true;
						console.log("Slider Value: " + value);
						var pos = value;
						var ctrlpacket = new CTRLPACKET(); //In JS as opposed to C/C++, using "new" without "delete" won't cause memory leak. Once function ends, the used memory space is dealt with by JS's garbage collector, putting back the used memory space into the freestore (heap)
						console.log(motorID + ': ' + pos);
						document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
						if(motorID == 'base'){
							ctrlpacket.base = pos;
							mimicPositions.base = pos;
						}
						else if(motorID == 'shlds'){
						//shoulder diabled until offset can be accounted for
							mimicPositions.shoulder = pos;
							ctrlpacket.shoulder = pos;
							//mimicPositions.shouldeL = ctrlpacket.shoulderL;
						}
						else if(motorID == 'elbow'){
							if(pos < 70){
								pos = 70;
							} else if (pos > 220){
								pos = 220;
							}
							ctrlpacket.elbow = pos;
							mimicPositions.elbow = pos;
						}
						else if(motorID == 'wrist'){
							if(pos < 120){
								pos = 120;
							} else if (pos > 240){
								pos = 240;
							}
							ctrlpacket.wrist = pos;
							mimicPositions.wrist = pos;
						}
						socket.emit("CTRLSIG", { directive: "ARM", info: ctrlpacket }); //send sig to server
						prevpos = pos;
						setTimeout(function(){
							delay = false;
						}, 10);
						/* Having CTRLPACKET as an object allows me to customize the size of data to be sent over to rover.
						For example: Suppose that I am scrolling for base motor only. A new object "ctrlpacket" is made,
						with only speed at first. Depending on which bar I am controlling, the if statements below will
						only add the attribute of the current motor being manipulated, so when the packet is sent, it will
						not include unnecessaryly large amounts of data. Scrolling base will only give out the packet with
						speed and base value only. Arm.js checks for which elements are present in packet, and since base
						and speed are all that's there, arm.js will only send out two control packets, one controlling this
						motor, and the other editting speed*/
					}
				});
				
		}
		else if($(this).hasClass("button-active")){ //is active at first
			// document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			$(this).removeClass("button-active").addClass("button-inactive"); //turns button off
			$('#scrl-' + motorID).hidden = true;
			// $('#form-' + motorID).hidden = true;
			// document.getElementById(motorID+"-status").innerHTML = "Not Detected";
			// $("#" + motorID + "-status").css("color", "red");
			$('scrl-' + $(this).val().toString()).prop("overflow-y", "hidden"); //deactivates scrollbar
			if($(this).hasClass("button-inactive")){
				$('.slide' + motorID).slider('destroy');
			}
		}
	});
}

function handleRoverSignal(data) {
	//feedback.log("INCOMING ROVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			//sensor.set(info);
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}
}
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
});

