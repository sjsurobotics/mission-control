var shiftPressed = false;
var controlPressed = false;
var isAtRest = false;
var isAtReady = false;
var isAimed = false;

$(document).ready(keyShortcuts); //wait for document to be ready before executing function

function keyShortcuts(){
	$(document).on('keydown', function(event){
		if(event.which == 16){ //key 'shift'
			shiftPressed = true;
			console.log("Shift pressed");
		}
		else if(event.which == 17){ //key 'ctrl'
			controlPressed = true;
			console.log("Ctrl pressed");
		}
		else if(event.which == 65 && !shiftPressed && !controlPressed){ //key 'a'
			$('#Grip').click(); //executes click event bound to button Grip
		}
		else if(event.which == 83 && !shiftPressed && !controlPressed){ //key 's'
			$('#Stop').click(); //executes click event bound to button Stop
		}
		else if(event.which == 68 && !shiftPressed && !controlPressed){ //key 'd'
			$('#Drop').click(); //executes click event bound to button Drop
		}
		else if(event.which == 70 && !shiftPressed && !controlPressed){ //key 'f'
			$('#Line').click(); //executes click event bound to button Line
		}
		else if(event.which == 84 && !shiftPressed && !controlPressed){ //key 't'
			if(!isAtRest && !isAtReady){
				$('#Aim').click(); //executes click event bound to button Grip
			}
		}
		else if(event.which == 82 && !shiftPressed && !controlPressed){ //key 'r'
			if(!isAtReady && !isAimed){
				$('#Rest').click(); //executes click event bound to button Rest
			}
		}
		else if(event.which == 69 && !shiftPressed && !controlPressed){ //key 'e'
			if(!isAtRest && !isAimed){
				$('#Ready').click(); //executes click event bound to button Ready
			}
		}
		else if(event.which == 87 && !shiftPressed && !controlPressed){ //key 'w'
			$('#Torque').click(); //executes click event bound to button Detorque
		}
		else if(event.which == 81 && !shiftPressed && !controlPressed){ //key 'q'
			$('#Reset').click(); //executes click event bound to button Reset
		}
	});
	$(document).on('keyup', function(event){
		if(event.which == 16){ //key 'shift'
			shiftPressed = false;
		}
		if(event.which == 17){ //key 'ctrl'
			controlPressed = false;
		}
	});
}
var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function(err) {
		alert(err.toString());
	}
};
function CTRLPACKET() { //prototype of packet to send to server then to Arm.handle()
	// this.base = number;
	// this.shoulder = number;
	// this.elbow = number;
	// this.wrist = number;
	// this.pump = string; //grip = deflate, stop = stop, drop = inflate
	// this.speed = string; //slow, normal, or fast
};
var stopServing = false;
var cnt = 0;
var initiallyPressed = false;
var prevpos = 0;
var mimicComm = false;
var mimicServer;
var prevID = '';
var protocol = {
	alignArm: { /*VALUES TO ALIGN ARM STRAIGT UP*/
		shoulder: 150, //shoulder elbow and wrist go straight up first
		elbow: 180,
		wrist: 150,
		base: 155 //Base moves to forward facing position
	},
	activateArm: {
		shoulder: 150,
		elbow: 150,
		wrist: 200,
		base: 155
	},
	restArm: {
		shoulder: 160, //referenced to Right Shoulder, moves arm to 180* back
		elbow: 230,
		wrist: 100,
		base: 155 //Base stays oriented at forward facing position. Shoulder, elbow, and wrist then lay down on top of rover
	},
	stdAim: {
		shoulder: 120,
		elbow: 105,
		wrist: 195,
		base: 150
	},
	speedSlow: {
		speed: "slow"
	},
	speedNorm: {
		speed: "normal"
	},
	speedFast: {
		speed: "fast"
	},
	torqueOff: {
		torque: "off"
	},
	torqueOn: {
		torque: "on"
	},
	examineRock: { /*'show to cam' VALUES NOT YET DETERMINED. FIND REAL VALUES ASAP*/
		shoulder: 150,
		elbow: 200,
		wrist: 150,
		base: 155
	},
}
var resetPacket = {
	reset: 0
};
var mimicPositions = {
	base: 150,
	shoulder: 150,
	elbow: 180,
	wrist: 150
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		shoulderL: 0,
		shoulderR: 0,
		elbow: 0,
		wrist: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res: 400,
		width: 640,
		height: 480,
		fps: 20
	}
};

var feedback = {
	log: function(str, obj) {
		if (typeof obj == "undefined") {
			console.log(str);
			$("#log").prepend(str + "\n");
		} else {
			console.log(str, obj);
			$("#log").prepend(str + " " + JSON.stringify(obj) + "\n");
		}
	}
};
var entity;
var socket = undefined;
var registered = false;
var getSensors = undefined;

/*Function Calls*/
motorCtrl();
gripperControl();
armProtocols();
speedControl();

$("#connect").on("click", function() {
	console.log("Clicked");
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', function(data) {
		feedback.log("INCOMING OCULARSIG", data);
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#cortex_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		$('.scrollbar').off('scroll');
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", {
		entity: entity,
		password: "destroymit"
	});
});
$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		location.reload(); //refresh page
	}
});

$(document).unbind('keydown').bind('keydown', function(event) {
	var doPrevent = false;
	if (event.keyCode === 8) {
		var d = event.srcElement || event.target;
		if ((d.tagName.toUpperCase() === 'INPUT' &&
				(
					d.type.toUpperCase() === 'TEXT' ||
					d.type.toUpperCase() === 'PASSWORD' ||
					d.type.toUpperCase() === 'FILE' ||
					d.type.toUpperCase() === 'EMAIL' ||
					d.type.toUpperCase() === 'SEARCH' ||
					d.type.toUpperCase() === 'DATE')
			) ||
			d.tagName.toUpperCase() === 'TEXTAREA') {
			doPrevent = d.readOnly || d.disabled;
		} else {
			doPrevent = true;
		}
	}
	if (doPrevent) {
		event.preventDefault();
	}
});

function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

function handleRoverSignal(data) {
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			//sensor.set(info);
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}
}

function speedControl(){
	$('#speedCtrl').slider({
		min: 1, 
		max: 3, 
		value: 2, 
		step: 1,
		reversed: true
	});
	$('#speedCtrl').on('slideStop', function(slideEvent){
		var value = slideEvent.value;
		if(value == 1){
			socket.emit("CTRLSIG", { directive: "ARM", info: protocol.speedSlow }); //send sig to server
			console.log("Slow Arm Speed");
			document.getElementById('armSpd').innerHTML = "Low";
		}
		else if(value == 2){
			socket.emit("CTRLSIG", { directive: "ARM", info: protocol.speedNorm }); //send sig to server
			console.log("Balanced Arm Speed");
			document.getElementById('armSpd').innerHTML = "Normal";
		}
		else if(value == 3){
			socket.emit("CTRLSIG", { directive: "ARM", info: protocol.speedFast }); //send sig to server
			console.log("Fast Arm Speed");
			document.getElementById('armSpd').innerHTML = "High";
		}
		else{
			console.log("Invalid Speed");
		}
	});
}

function armProtocols(){
	var sequenceDelay = 3000; //amount of time (ms) before secondary protocols are sent
	$('#Aim').on('click', function(event){
		var parent = this;
		if(this.value == "NA"){
			$('#Ready').prop("disabled", true);
			$('#Rest').prop("disabled", true);
			stopServing = true;
			isAimed = true;
			$(this).val("Busy");
			console.log("Putting On Aim");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive: "ARM", info: protocol.stdAim});
			setTimeout(function(){
				$(parent).css("background-color", "red");
				$(parent).val("Aimed");
				setTimeout(function (){
					stopServing = false;
					console.log("Ready to serve");
				}, 500);
			}, sequenceDelay);
		}
		else if(this.value = "Aimed"){
			stopServing = true;
			$(this).val("Busy");
			console.log("Aligning Arm");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive: "ARM", info: protocol.alignArm});
			setTimeout(function(){
				$(parent).css("background-color", "black");
				$(parent).val("NA");
				$('#Ready').prop("disabled", false);
				$('#Rest').prop("disabled", false);
				isAimed = false;
				setTimeout(function (){
					stopServing = false;
					console.log("Ready to serve");
				}, 500);
			}, sequenceDelay);
		}
		else if(this.value == "Busy"){
			console.log("Currently running a process!");
		}
		else{
			console.log("Bad Protocol");
		}
	});
	$('#Reset').on('click', function(event){ //Torque Setting button (to unshutdown)!!!
		socket.emit("CTRLSIG", {directive: "ARM", info: resetPacket});
		console.log("Reset Torque Sent!");
	});
	$('#Rest').on('click', function(event){ //Rest/Basket Position Button
		var parent = this;
		if(this.value == "Active"){ //If arm is active and needs to rest
			$('#Ready').prop("disabled", true);
			$('#Aim').prop("disabled", true);
			isAtRest = true;
			stopServing = true;
			this.value = "Busy"; //If currently running rest sequence, let user know
			console.log("Resting Arm!");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive:"ARM", info: protocol.alignArm}); //Arm straight up
			setTimeout(function(){
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.restArm});
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.restArm});
				$(parent).css("background-color", "red");
				$('#Rest').val("At Rest");
			}, sequenceDelay);
		}
		else if(this.value == "At Rest"){ //button deactivates
			this.value = "Busy"; //If currently running activation sequence, let user know
			console.log("Activating Arm!");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive:"ARM", info: protocol.alignArm});
			setTimeout(function(){
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.activateArm});
				$(parent).css("background-color", "black");
				$('#Rest').val("Active");
				$('#Ready').prop("disabled", false);
				$('#Aim').prop("disabled", false);
				isAtRest = false;
				stopServing = false;
				console.log("Ready to serve");
			}, sequenceDelay);
		}
		else if(this.value == "Busy"){
			console.log("Currently running a process!");
		}
		else{
			console.log("Bad protocol");
		}
	});
	$('#Ready').on('click', function(){ //Ready Button
		var parent = this;
		if(this.value == "Standby"){ //button activates
			$('#Rest').prop("disabled", true);
			$('#Aim').prop("disabled", true);
			stopServing = true;
			isAtReady = true;
			this.value = "Busy"; //If currently running examine sequence, let user know
			console.log("Getting Ready!");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive:"ARM", info: protocol.alignArm});
			setTimeout(function(){
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.activateArm});
				$(parent).css("background-color", "red");
				$('#Ready').val("Ready");
				setTimeout(function (){
					stopServing = false;
					console.log("Ready to serve");
				}, 500);
			}, sequenceDelay);
		}
		else if(this.value == "Ready"){ //button deactivates
			stopServing = true;
			this.value = "Busy"; //If currently positioning arm, wait before commands are sent
			console.log("Straightening Arm!");
			$(this).css("background-color", "gray");
			socket.emit("CTRLSIG", {directive:"ARM", info: protocol.alignArm});
			setTimeout(function(){
				// socket.emit("CTRLSIG", {directive:"ARM", info: protocol.activateArm});
				$(parent).css("background-color", "black");
				$('#Ready').val("Standby");
				$('#Rest').prop("disabled", false);
				$('#Aim').prop("disabled", false);
				isAtReady = false;
				setTimeout(function (){
					stopServing = false;
					console.log("Ready to serve");
				}, 500);
			}, sequenceDelay);
		}
		else if(this.value == "Busy"){
			console.log("Currently running a process!");
		}
		else{
			console.log("Bad protocol");
		}
	});
	$('#Torque').on('click', function(){ //Torque Toggle Button
		var parent = this;
		if(this.value == "Torque"){ //Torque shutdown is instantaneous
			$(this).css("background-color", "gray");
			$('#Line').val("offline"); //Since torque deactivation clears line interval, set line button to "offline"
			document.getElementById("Line").innerHTML = "Offline(F)";
			console.log("Deactivating Torque!");
			this.value = "Busy";
			setTimeout(function(){
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.torqueOff});
				$(parent).css("background-color", "red");
				parent.value = "NoTorque";
			}, 750);
		}
		else if(this.value == "NoTorque"){
			$(this).css("background-color", "gray");
			console.log("Activating Torque!");
			this.value = "Busy";
			setTimeout(function(){
				socket.emit("CTRLSIG", {directive:"ARM", info: protocol.torqueOn});
				$(parent).css("background-color", "black");
				parent.value = "Torque";
			}, 750);
		}
		else if(this.value == "Busy"){
			console.log("Currently running a process!");
		}
		else{
			console.log("Bad protocol");
		}
	});
}

function gripperControl(){ //controls the gripper
	$('.gripper').on('click', function(event){
		var ctrlpacket = new CTRLPACKET();
		if($(this).val() == "grip"){
			console.log("Gripping!");
			ctrlpacket.pump = $(this).val();
		}
		else if($(this).val() == "stop"){
			console.log("Stopping Grip!");
			ctrlpacket.pump = $(this).val();
		}
		else if($(this).val() == "drop"){
			console.log("Dropping!");
			ctrlpacket.pump = $(this).val();
		}
		else if($(this).val() == "offline"){
			console.log("Setting interval");
			ctrlpacket.line = "online";
			$(this).val("online");
			document.getElementById("Line").innerHTML = "Online(F)";
		}
		else if($(this).val() == "online"){
			console.log("Clearing interval");
			ctrlpacket.line = "offline";
			$(this).val("offline");
			document.getElementById("Line").innerHTML = "Offline(F)";
		}
		socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});
		console.log("Sent " + $(this).val() + "Command!");
	});
}

function motorCtrl(){
	feedback.log("Press the Control Type Button to activate Control!!!");
	//Input Toggle: Switches input Type
	$('#controltype').removeClass("btn-success").addClass("btn-danger");
	$('#controltype').css("background-color", "black");
	$('#controltype').on('click', function(event){
		if(!initiallyPressed){
			$(this).removeClass("btn-danger").addClass("btn-success");
			$('#controltype').css("background-color", "limegreen");
			$(this).val("Mimic"); //set to Mimic to start in scrollbar mode, but inactive
			$('.txtbox').prop("disabled", true);
			$('.gripper').prop("disabled", false);
			$('.protocolPreset').prop("disabled", false);
			initiallyPressed = true;
		}
		if($(this).val() == "Scrollbar"){
			$(this).val("Text Entry"); //switch control type
			document.getElementById('controltype').innerHTML = "Text Entry";
			$('.motor > button').off('click');
			//Kill the sliders
			$('.slide').slider('destroy');
			$('.slide').prop("hidden", true);
			$('#armSpd').prop("hidden", false);
			$('.scrollbar').prop('disabled', true);
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			//Hide Text Inputs
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", false);
			$('.txtbutton').prop('hidden', false);
			TextControl();
		}
		else if($(this).val() == "Text Entry"){
			$(this).val("Mimic"); //switch control type
			document.getElementById('controltype').innerHTML = "Mimic";
			if(mimicComm == true){
				clearInterval(mimicServer);
				mimicComm = false;
			}
			$('.motor > button').off('click');
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			//Hide Text Inputs
			$('#armSpd').prop("hidden", false);
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", true);
			$('.txtbutton').prop('hidden', true);
			$('.txtbutton').prop('disabled', true);
			$('.txtbutton').unbind();
			MimicControl();
		}
		else if($(this).val() == "Mimic"){
			$(this).val("Scrollbar"); //switch control type
			document.getElementById('controltype').innerHTML = "Scrollbar";
			if(mimicComm == true){
				clearInterval(mimicServer);
				mimicComm = false;
			}
			//Hide Text Inputs & kill the sliders
			$('.slide').prop("hidden", true);
			$('#armSpd').prop("hidden", false);
			$('.txtbox').prop("disabled", true);
			$('.txtbox').prop("hidden", true);
			$('.txtbutton').prop('hidden', true);
			$('.motor > button').removeClass("button-active").addClass("button-inactive");
			ScrollbarControl();
		}
	});
}

function mimicHandler(data, status){ //ajax request sent by apache server, and server receives data from test.txt
	console.log("Request Successful...");
	var inputPacket = JSON.parse(data); //Parses test.txt's data to a JSON object
	var valid_packet = false;
	var ctrlpacket = {
		shoulder: mimicPositions.shoulder,
		elbow: mimicPositions.elbow,
		wrist: mimicPositions.wrist,
		base: mimicPositions.base
	};

	if(typeof inputPacket["shoulder"] != "undefined"){
		if(inputPacket["shoulder"] != mimicPositions.shoulder){ 
			var valid_packet = true;
			var pos = inputPacket["shoulder"];
			if(pos < 45){pos = 45;} else if(pos > 180){pos = 180;} //angular limiter
			mimicPositions.shoulder = inputPacket["shoulder"];
			ctrlpacket.shoulder = inputPacket["shoulder"];
			$('#deg-shlds').html(pos.toString());
		}
	}
	if(typeof inputPacket["elbow"] != "undefined"){ 
		if(inputPacket["elbow"] != mimicPositions.elbow){
			var valid_packet = true;
			var pos = inputPacket["elbow"] + 30;
			if(pos < 100) { pos = 100; }else if(pos > 235) { pos = 235; } //angular limiter
			ctrlpacket.elbow = pos;
			mimicPositions.elbow = pos;
			$('#deg-elbow').html(pos.toString());
		}
	}
	if(typeof inputPacket["wrist"] != "undefined"){
		if(inputPacket["wrist"] != mimicPositions.wrist){
			var valid_packet = true;
			var pos = inputPacket["wrist"];
			if(pos < 100){ pos = 100; }else if(pos > 240) { pos = 240; } //angular limiter
			ctrlpacket.wrist = pos;
			mimicPositions.wrist = pos;
			$('#deg-wrist').html(pos.toString());
		}
	}
	if(typeof inputPacket["base"] != "undefined"){
		if(inputPacket["base"] != mimicPositions.wrist){
			var valid_packet = true;
			var pos = inputPacket["base"] + 40;
			if(pos < 100){ pos = 100; }else if(pos > 200) { pos = 200; } //angular limiter
			ctrlpacket.base = pos;
			mimicPositions.base = pos;
			$('#deg-base').html(pos.toString());
		}
	}
	if(valid_packet && !stopServing) {
		socket.emit("CTRLSIG",{directive: "ARM", info: ctrlpacket});	
	}
}

function MimicControl(){
	console.log("Mode: Mimic");
	mimicComm = true;

	mimicServer = setInterval(function() { //Text Request
		console.log("Inside setInterval");
		$.ajax({
			url: "http://127.0.0.1:8500/",
			dataType: 'text',
			cache: false,
			success: mimicHandler,
	        error: function(jqXHR, textStatus, errorThrown) {
	        	console.log("error!");
			}
		});
	}, 100);
}

function TextControl(){
	console.log("Mode: Text Entry");
	if(mimicComm == true) {
		clearInterval(mimicServer);
		mimicComm = false;
	}
	$('.motor > button').on('click', function(event){ //JQuery: for all elements of class 'motor' and type 'button' (i.e. motor control buttons)...
		var motorID = $(this).val().toString();
		if($(this).hasClass("button-inactive")){ //is inactive at first
			$(this).removeClass("button-inactive").addClass("button-active"); //turns button on
			$('#txtbutton' + motorID).prop('disabled', false);
			console.log("motorID: " + motorID);
			console.log(CTRLPACKET);
			/*Text Entry Mode*/
			$('#txt' + motorID).prop('disabled', false); //enable text box
			$('#form-' + motorID).keydown(function(event){
				if(event.keyCode == 13){
					event.preventDefault();
					console.log("prevented submission");
				}
			});
			$('#txtbutton' + motorID).on('click', function(event){
				var pos = Number($('#txt' + motorID).val().toString()); //Take in degree value
				//Handle Degree Overflow
				if(pos > 360){
					pos = 360;
					$('#txt' + motorID).val("360");
				}
				else if(pos < 0){
					pos = 0;
					$('#txt' + motorID).val("0");
				}
				//Put packet together
				var ctrlpacket = new CTRLPACKET();
				if(motorID == 'base' && pos != mimicPositions.base){
					if(pos < 240){ pos = 240; }else if(pos > 340) { pos = 340; } //angular limiter
					ctrlpacket.base = pos;
					mimicPositions.base = pos;
				}
				else if(motorID == 'shlds'){ //Max shdl. R val is 180 dg for RX64
					if(pos < 45){pos = 45;} else if(pos > 180){pos = 180;} //angular limiter
					if(pos != mimicPositions.shoulderR){
						ctrlpacket.shoulderR = pos;
						ctrlpacket.shoulderL = -1*(pos - 300);
						console.log("sL-" + ctrlpacket.shoulderL + " sR-" + ctrlpacket.shoulderR);
						mimicPositions.shoulderR = pos;
						mimicPositions.shoulderL = ctrlpacket.shoulderL;
					}
				}
				else if(motorID == 'elbow' && pos != mimicPositions.elbow){
					if(pos < 70) { pos = 70; }else if(pos > 205) { pos = 205; } //angular limiter
					ctrlpacket.elbow = pos;
					mimicPositions.elbow = pos;
				}
				else if(motorID == 'wrist' && pos != mimicPositions.wrist){
					if(pos < 100){ pos = 100; }else if(pos > 240) { pos = 240; } //angular limiter
					ctrlpacket.wrist = pos;
					mimicPositions.wrist = pos;
				}
				if(!stopServing){
					socket.emit("CTRLSIG", {directive:"ARM", info: ctrlpacket});
					prevpos = pos;
					prevID = motorID;
				}
				document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
			});
		}
		else if($(this).hasClass("button-active")){ //is active at first
			$('#txtbutton' + motorID).prop('disabled', true);
			$(this).removeClass("button-active").addClass("button-inactive"); //turns button off
			$('#txt' + motorID).prop('disabled', true); //disable text box
			$('#txtbutton' + motorID).off('click');
			$('.scrollbar').off('slide'); //for sliders
		}
	});
}
var delay = false;

function ScrollbarControl(){
	console.log("Mode: Scrollbar");
	if(mimicComm == true){
		clearInterval(mimicServer);
		mimicComm = false;
	}
	$('.motor > button').on('click', function(event){
		var motorID = $(this).val().toString();
		if($(this).hasClass("button-inactive")){ //is inactive at first
			$(this).removeClass("button-inactive").addClass("button-active"); //turns button on
			$('.slide' + motorID).prop("hidden", false);
			console.log("motorID: " + motorID);
			console.log(CTRLPACKET);
			/*Scrollbar Mode*/
				if(motorID == 'base'){
					$('#slide-base').slider({min: 100, max: 200, value: 150, step: 5});
				}
				else if(motorID == 'shlds'){
					$('#slide-shlds').slider({min: 45, max: 180, value: 150, step: 5});
				}
				else if(motorID == 'elbow'){
					$('#slide-elbow').slider({min: 100, max: 235, value: 180, step: 5});
				}
				else if(motorID == 'wrist'){
					$('#slide-wrist').slider({min: 100, max: 240, value: 150, step: 5});
				}
				$('#slide-' + motorID).on('slideStop',/*min:0, max:360, value:150, step: 5,*/
					function(slideEvent){
						var value = slideEvent.value;
						if(delay) { return; }
						delay = true;

						console.log("Slider Value: " + value);
						var pos = value;
						var ctrlpacket = {
							shoulder: mimicPositions.shoulder,
							elbow: mimicPositions.elbow,
							wrist: mimicPositions.wrist,
							base: mimicPositions.base
						};
						 //In JS as opposed to C/C++, using "new" without "delete" won't cause memory leak. Once function ends, the used memory space is dealt with by JS's garbage collector, putting back the used memory space into the freestore (heap)
						console.log(motorID + ': ' + pos);
						document.getElementById("deg-" + motorID).innerHTML = pos; //Angle Indicator
						if(motorID == 'base'){
							if(pos < 100){ pos = 100; } else if(pos > 200) { pos = 200; } //angular limiters
							ctrlpacket.base = pos;
							mimicPositions.base = pos;
						}
						else if(motorID == 'shlds'){
							if(pos < 45){pos = 45;} else if(pos > 180){pos = 180;} //angular limiter
							mimicPositions.shoulder = pos;
							ctrlpacket.shoulder = pos;
						}
						else if(motorID == 'elbow'){
							if(pos < 100) { pos = 100; } else if(pos > 235) { pos = 235; } //angular limiters

							ctrlpacket.elbow = pos;
							mimicPositions.elbow = pos;
						}
						else if(motorID == 'wrist'){
							if(pos < 100){ pos = 100; } else if(pos > 240) { pos = 240; } //angular limiters
							ctrlpacket.wrist = pos;
							mimicPositions.wrist = pos;
						}
						if(!stopServing){
							socket.emit("CTRLSIG", { directive: "ARM", info: ctrlpacket }); //send sig to server
						}
						prevpos = pos;
						setTimeout(function(){
							delay = false;
						}, 10);
					}
				);
				
		}
		else if($(this).hasClass("button-active")){ //is active at first
			$(this).removeClass("button-active").addClass("button-inactive"); //turns button off
			if($(this).hasClass("button-inactive")){
				$('.slide' + motorID).prop("hidden", true);
				$('.slide' + motorID).slider('destroy');
			}
		}
	});
}

$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
});
