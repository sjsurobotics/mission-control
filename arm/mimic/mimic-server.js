var app = require('express')();
var fs = require('fs');
var serialport = require("serialport");
var SerialPort = serialport.SerialPort;

var i;
var path = "/dev/ttyACM0";
var found = false;
var positions = {
	base: 180,
	shoulder: 180,
	elbow: 180,
	wrist: 180
};
var first_input = false;

for(i = 10; i >= 0; i--) {
	path = "/dev/ttyACM"+i;
	if (fs.existsSync(path)) {
		found = true;
		console.log("Arduino found on "+path);	
		break;
	}
	console.log("Could not find "+path);	
}
if(!found) {
	console.log("No arduino found, quiting application.");
	process.kill();
}

var serialPort = new SerialPort(path, {
	baudrate: 9600,
	parser: serialport.parsers.readline("\n")
});
var buffer = "";
serialPort.on("open", function () {
	console.log('Connected to '+path);
	serialPort.on('data', function(data) {
		console.log('data received: ' + data);
		first_input = true;
		data = data.toString();
		var angles = data.split(",");
		positions["base"] = parseInt(angles[0]);
		positions["shoulder"] = parseInt(angles[1]);
		positions["elbow"] = parseInt(angles[2]);
		positions["wrist"] = parseInt(angles[3]);
	});
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', function (req, res) {
	var pos = JSON.stringify(positions);
	console.log(pos);
	res.send(pos);
});

var server = app.listen(8500, function () {
  var host = server.address().address;
  var port = server.address().port;
  console.log('App listening at http://127.0.0.1:%s', port);
});
