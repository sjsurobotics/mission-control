var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function(err) {
		alert(err.toString());
	}
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		shoulderL: 0,
		shoulderR: 0,
		elbow: 0,
		wrist: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res: 400,
		width: 640,
		height: 480,
		fps: 20,
		stream: 0
	},
	AUDIO: {
		mic: 2,
		bitrate: 96,
		stream: 0,
	}
};
var editor = new JSONEditor(document.querySelector("#info"), options, schemas["MOTOR"]);
var sensor = new JSONEditor(document.querySelector("#sensor"), options);
var feedback = {
	log: function(str, obj) {
		if (typeof obj == "undefined") {
			console.log(str);
			$("#log").prepend(str + "\n");
		} else {
			console.log(str, obj);
			$("#log").prepend(str + " " + JSON.stringify(obj) + "\n");
		}
		// keep the log on the bottom
		//var textarea = document.getElementById('log');
		//textarea.scrollTop = textarea.scrollHeight;
	}
};
var entity;
var socket = undefined;
var registered = false;
var getSensors = undefined;
//// VIDEO FEED
var mcanvas = document.getElementById('multi-cam');
var mctx = mcanvas.getContext('2d');

mctx.fillStyle = '#CCC';
mctx.fillText('... WAITING FOR SERVER ...', mcanvas.width / 2 - 60, mcanvas.height / 2);

// Setup the WebSocket connection and start the player
//var mclient = new WebSocket( 'ws://discovery.srkarra.com:9000/' );
var mclient, mplayer;
//// DOM Event listeners
$("#connect").on("click", function() {
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
		mclient = new WebSocket('ws://' + address + ':9000/');
		mplayer = new jsmpeg(mclient, {
			canvas: mcanvas
		});
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', function(data) {
		feedback.log("INCOMING OCULARSIG", data);
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#core_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", {
		entity: entity,
		password: "destroymit"
	});
});
$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		// //socket.disconnect();
		// socket.io.close();
		// socket = null;
		// socket = undefined;
		// registered = false;
		location.reload();
	}
});
$("#ctrl").on("click", function() {
	if (!registered) { return; }
	var directive = $("#directive").val();
	var info = editor.get();
	if (info == "") {
		bootbox.alert("You must send information with your directive.");
	} else {
		socket.emit("CTRLSIG", {
			directive: directive,
			info: info
		});
	}
});




$("#directive").on("change", function() {
	editor.set(schemas[$(this).val()]);
});

function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

function handleRoverSignal(data) {
	//feedback.log("INCOMING ROVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			sensor.set(info);
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}

}
$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }


});



