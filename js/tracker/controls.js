/*
Simple Interface script to get access to the pitch, yaw, zoom and Google map values of the GUI components
*/



function getAngles() {
	console.log("Begin transmitting angle directives...");
	//while(calculating);
	console.log("Sending angle directives: Pitch: "+  getPitch() + "  Yaw: "+ getYaw());
	return {pitch: getPitch(),
			yaw: getYaw()};
}

function deleteCoord() {
	if(typeof gpsX == undefined) return;
	var circleToDelete;
	for(var i = 0; i < circles.length; i++) {
		if(circles[i].contains(gpsX, gpsY)) {
			circleToDelete = circles[i];
			circles.splice(i, 1);
			break;
		}
	}

}

function getZoom() {
	return zoom;
}