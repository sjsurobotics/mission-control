var container = document.getElementById('jsoneditor');
var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function (err) {
	  alert(err.toString());
	}
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		sholder: 0,
		joint: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res:  400,
		width: 640,
		height: 480,
		fps: 20
	}
};
var editor = new JSONEditor(container, options, schemas["MOTOR"]);
var feedback = {
	log: function(str, obj) {
		if(typeof obj == "undefined") {
			console.log(str);
			$("#log").append(str+"\n");
		} else {
			console.log(str, obj);
			$("#log").append(str+" "+JSON.stringify(obj)+"\n");
		}
	},
	test: "hello"
};
var entity;
var socket = undefined;
var registered = false;
//// DOM Event listeners
$("#connect").on("click", function() {
	var address = $("#server").val();
	if(!_.isUndefined(socket)) {
		socket = undefined;
		console.log("Forcing new connection.");
		socket = io('http://'+address+':8085', {forceNew: true});
	} else {
		socket = io('http://'+address+':8085');
	}
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("color", "green");
		$("#server_status").text("Available");
	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', function(data){
		feedback.log("INCOMING ROVERSIG", data); 
		switch (data["directive"]) {
				case "TRACKER" :
					var pitch = data["info"]["pitch"];
					var yaw = data["info"]["yaw"];
					console.log(pitch + " " + yaw);
					calculateGPS(pitch, yaw);
					break;
				default:
					console.log("Invalid directive ", data["directive"]);
					break;
			}
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', function (data) {
		feedback.log("INCOMING SERVERSIG", data);
		if(_.isString(data)) {
			switch(data) {
				case "REGISTATION_COMPLETE":
					$("#mc_status").css("color", "green");
					$("#mc_status").text("Connected");
					registered = true;
					break;
				case "PASSWORD_INCORRECT": 
					$("#mc_status").css("color", "red");
					$("#mc_status").text("Disconnected");
					break;
				case "ROVER_CONNECTED":
					feedback.log("ROVER CONNECTED TO SERVER"); 
					$("#rov_status").css("color", "green");
					$("#rov_status").text("Connected");
					break;
				case "ROVER_DISCONNECTED":
					feedback.log("ROVER DISCONNECTED FROM SERVER"); 
					$("#rov_status").css("color", "red");
					$("#rov_status").text("Disconnected");
					break;
			}
		}
	});
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#rov_status").css("color", "red");
		$("#rov_status").text("Disconnected");
		$("#mc_status").css("color", "red");
		$("#mc_status").text("Disconnected");
		$("#server_status").css("color", "red");
		$("#server_status").text("Unavailable");
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", { entity: entity, password: "destroymit" });
});

$("#disconnect").on("click", function() {
	if(typeof socket != "undefined") {
		socket.disconnect();
	}
});

$("#ctrl").on("click", function() {
	if(!registered) { return; }
	var directive = $("#directive").val();
	var info = editor.get();
	if(info == "") {
		socket.emit("CTRLSIG", { directive: directive });
	} else {
		socket.emit("CTRLSIG", { directive: directive, info: info });
	}
});

$("#gpsCtrl").on("click", function() {
	if(!registered) return;
	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Get Rotations"}});
});

$("#coord-delete").on("click", function() {
	if(!registered) return;
	deleteCoord();
});

$(".control-panel").on("click", function() {
	if(!registered) return;
	var data = getAngles();
	console.log("Sending angle directives");
	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Rotations", pitch: data.pitch, yaw: data.yaw}});
});

$("#zoomCanvas").on("mousemove", function() {
	if(!registered) return;
	var zoom = getZoom();
	console.log("Sending zoom directives.");
	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Zoom", zoom: zoom}});
})

$("#directive").on("change", function() {
	editor.set(schemas[$(this).val()]);
});
//// VIDEO FEED
// var mcanvas = document.getElementById('multi-cam');
// var mctx = mcanvas.getContext('2d');

// mctx.fillStyle = '#CCC';
// mctx.fillText('... WAITING FOR SERVER ...', mcanvas.width/2-60, mcanvas.height/2);

// // Setup the WebSocket connection and start the player
// //var mclient = new WebSocket( 'ws://discovery.srkarra.com:9000/' );
// var mclient = new WebSocket( 'ws://127.0.0.1:9000/' );
// var mplayer = new jsmpeg(mclient, {canvas:mcanvas});
