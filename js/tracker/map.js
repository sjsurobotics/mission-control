var map;
var rocks;
var centerPos;
var roverPos;

function initMap() {
	centerPos = new google.maps.LatLng(29.564887, -95.081316);
	var minZoomLevel = 19;
	var mapOpts = {
		center : centerPos,
		zoom : 19,
		zoomControl: false,
		maxZoom : 19,
		panControl : false,
		mapTypeId : google.maps.MapTypeId.SATELLITE,
		streetViewControl : false,
		draggable : false
	};
	map = new google.maps.Map(document.getElementById("map_div"), mapOpts);

	// Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });

   var image = {
   	url : 'http://i57.tinypic.com/2zsa249.png',
   	scaledSize: new google.maps.Size(50,50)
   }
   roverPos = new google.maps.Marker({
   		map : map,
   		position : centerPos,
   		draggable : true,
   		icon : image,
   		title : "ROVER " + centerPos.lat() + " " + centerPos.lng()
   });

   google.maps.event.addListener(roverPos, 'drag', function() {
   		center = [roverPos.getPosition().lat(), roverPos.getPosition().lng()];
   });
}
//<a href="http://tinypic.com?ref=m8ntja" target="_blank"><img src="http://i58.tinypic.com/m8ntja.png" border="0" alt="Image and video hosting by TinyPic"></a>
function addMarker(point) {
	var LatLng = new google.maps.LatLng(point[0], point[1]);
	//var image = 'js/tracker/diamond.png';
	var image = {
	    url: 'http://i60.tinypic.com/1e3rk8.jpg',
	    // // This marker is 20 pixels wide by 32 pixels tall.
	     scaledSize: new google.maps.Size(50, 50),
	    // // The origin for this image is 0,0.
	    // origin: new google.maps.Point(0,0),
	    // // The anchor for this image is the base of the flagpole at 0,32.
	    // anchor: new google.maps.Point(10, 32)
  	};

	var marker = new google.maps.Marker({
		map : map,
		position : LatLng,
		draggable : true,
		icon : image,
		title : "ROCK " + LatLng.lat() + " " + LatLng.lng()
	});

	google.maps.event.addListener(marker, 'dblclick', function() {
		marker.setMap(null);
	});

	google.maps.event.addListener(marker, 'click', function() {
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
	});
	google.maps.event.addListener(marker, 'drag', function() {
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
	});

}