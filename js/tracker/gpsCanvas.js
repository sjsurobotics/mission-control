/**
*gpsCanvas.js manages the minimap of the Johnson Space Center. This script manages
*the blinking lights on the minimap.
**/

//Canvas elements
var canvas;   		//Canvas element with height and width properties
var cnvsHeight = 500;
var cnvsWidth = 500;
var ctx;
var color;		//This color will go back and forth between blue and white to create blinking effect
var circles;   //An array to hold refs to each Circle object
var dragging;  
var blinkRadius;   //radius of the blinking dots
var blinkTime = 350;
var selectedCircle;   //Is set when we click and drag a circle

//GPS calculations
var centerX;  //Center of the canvas element
var centerY;
var roverLength;
var roverRot = 145; //Rover's roation
var center;   	//Rover's GPS coordinates   -- right now it is static. Will need to change once the GPS module is up and running
var coords;		//List of coordinates of interest
var height;		//Height of camera mast, used in trig calculations
var trueYaw;

var roverLat;
var roverLong;

function init() {
	//Do some canvas setup
	//canvas = document.getElementById("gpsCanvas");
	// ctx = canvas.getContext("2d");
	// canvas.height = cnvsHeight;
	// canvas.width = cnvsWidth;

	//createGrid();
	

	//var curPos = findPos(canvas);  -- This bit of code is supposed to account for DOM element offset, but it doesnt work so fuck it.
	centerX =   cnvsWidth/2;  
	centerY = cnvsHeight/2;
	console.log("Center x: " + centerX + " Center y: "+ centerY);  //Checking where the rover is

	roverLat = 29.564887;
	roverLong = -95.081316;
	
	//Initializing the variables above
	circles = [];   
	center = [roverLat, roverLong];
	coords = [];
	height = 3;
	blinkRadius = 16;
	roverLength = blinkRadius * 1.5;
	color = "Navy";

	//drawRover();
	//Set the timer for blink function
	//setInterval(blink, blinkTime);

	// canvas.addEventListener("mousedown", function(evt) {
	// 	if( containsCircle(evt) ) {
	// 		displayGPS();
	// 		canvas.addEventListener("mousemove", mouseMoveListener, false);
	// 	}

	// }, false);

	// canvas.addEventListener("mouseup", function(evt) {
	// 	if(selectedCircle) {
	// 		console.log("canceling selectedCircle");
	// 		dragging = false;
	// 		selectedCircle.isDragging = false;
	// 		selectedCircle = null;
	// 		canvas.removeEventListener("mousemove", mouseMoveListener);
	// 	}	
	// }, false);
	
}


function displayGPS() {
	//ctx.clearRect(0,canvas.height - 25, cnvsWidth, 25);
	ctx.font = "15px sans-serif";
	ctx.textAlign = "left";
	ctx.textBaseline = "bottom";
	ctx.fillStyle = "#000000";
	ctx.fillText("GPS Coordinate at:  Lat: " + gpsX + "  Long: " + gpsY, 10 , canvas.height - 10);
}
/*
Draws the rover triangle at center[0] and center[1]

*/
function drawRover() {
	ctx.save();
	
	ctx.fillStyle = "PaleVioletRed";
	ctx.translate(center[0], center[1]);
	ctx.rotate(toRads(-roverRot));

	//Tip of triangle
	ctx.beginPath();
	ctx.moveTo(roverLength, 0);

	//Second point
	ctx.rotate(toRads(-120));
	ctx.lineTo(roverLength, 0);

	//Third point
	ctx.rotate(toRads(-60));
	ctx.lineTo(roverLength/8,0);

	//Fourth point
	ctx.rotate(toRads(-60));
	ctx.lineTo(roverLength, 0);

	//Back to tip
	ctx.rotate(toRads(-120));
	ctx.lineTo(roverLength, 0);
	ctx.fill();

	
	ctx.restore();

}

/*
Draws the rectangle grid onto the canvas element
*/
function createGrid() {
	ctx.lineWidth = 1;
	for(var x = .5; x < canvas.width; x += 10) {

		
		ctx.beginPath();
		ctx.strokeStyle = "#DDDDDD";
		ctx.moveTo(x, 0);
		ctx.lineTo(x, canvas.height);
		ctx.stroke();
	}
	for(var y = .5; y < canvas.width; y += 10) {
		ctx.beginPath();
		ctx.strokeStyle = "#DDDDDD";
		ctx.moveTo(0, y);
		ctx.lineTo(canvas.width, y);
		ctx.stroke();
	}
}
/*
This function is supposed to check for DOM element offset, but it just fucks everything up.
*/
function findPos(obj) {
    var curleft = 0, curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { x: curleft, y: curtop };
    }
    return undefined;
}
/*
This is the mosue move listener that only gets fired if someone clicks and drags a 
blinking dot within the canvas element.
*/
function mouseMoveListener(e) {
	var x = e.offsetX;
	var y = e.offsetY;
	if(dragging) {
		selectedCircle.move(x, y);
		gpsX = selectedCircle.x;
		gpsY = selectedCircle.y;
		displayGPS();
	}
}
/*
Returns whether or not a mousedown event hit a blinking dot
*/
function containsCircle(e) {
	var x = e.offsetX;
	var y = e.offsetY;
	var hit = false;;
	for(var i = 0; i < circles.length; i++) {
		if(circles[i].contains(x, y)) {
			console.log("Got one!");
			selectedCircle = circles[i];
			selectedCircle.isDragging = true;
			dragging = true;
			hit = true;
			gpsX = circles[i].x;
			gpsY = circles[i].y;
		}
	}
	return hit;
}

/*
Calculates the coordinates of a point of interest given the pitch and yaw of the camera mast.
Right now, the function does not take into account the rover's dynamic orientation. Again,
this will be updated when the GPS module is up and running.

@param 	pitch	Value of the pitch of the camera mast
@param 	Yaw 	Value of the yaw   of the camera mast
@return void    
*/
function calculateGPS(pitch, yaw) {

	console.log("Calculating GPS coordinate at pitch: " + pitch + "  Yaw: " + yaw);
	var dist = height * Math.tan(pitch * Math.PI / 180);

	trueYaw = (yaw - 90) + roverRot;
	console.log(dist);
	var dLat = dist/(370800) * Math.sin(toRads(trueYaw));
	var dLong = dist/(370800) * Math.cos(toRads(trueYaw));

	//Here is where we will need to take into account the rover's orientation
	var point = [center[0] + dLat, center[1] + dLong];
	addMarker(point);
	coords[coords.length] = point;
	console.log("Coordinate point: " + point[0] + " " + point[1]);
	circles[circles.length] = new Circle(point[0], point[1], color);
	
}
/*
This is the animating function that creates the blinking effect of each point of interest.
*/
function blink() {
	if(color == "gray"){ color = "Navy"; }
	else  { color = "gray"; }
	canvas.width = canvas.width;
	createGrid();
	for(var i = 0; i < circles.length; i++) {

		if(!circles[i].isDragging)
			circles[i].blink(color);
		else
			circles[i].fill = "SkyBlue";
		circles[i].draw(ctx);
	}
	drawRover();
	displayGPS();
	
}

//============== Circle Class Below ===========//

/*
Constructor for Circle object. The radius and isDragging variables are set
independently of the passed parameters

@param 	x 	X-coordinate of origin
@param 	y 	Y-coordinate of origin
@param 	fill 	Initial fill color
*/
function Circle(x, y, fill) {
	this.x = x;
	this.y = y;
	this.fill = fill;
	this.radius = blinkRadius;
	this.isDragging = false;
}

/*
Draws the Circle object to the canvas.

@param  	ctx 		A 2d canvas context on which to draw
*/
Circle.prototype.draw = function(ctx) {
	ctx.fillStyle = this.fill;
	ctx.beginPath();
	ctx.arc(this.x, this.y, this.radius, 0, Math.PI *2);
	ctx.closePath();
	ctx.fill();
}

/*
Changes the color of this circle

@param 	color	Color to change to
*/
Circle.prototype.blink = function(color) {
	this.fill = color;
}
/*
Moves the circle to the given coordinates
@param x 	New x-coord
@param y 	New y-coord
*/
Circle.prototype.move = function(x, y) {
	this.x = x;
	this.y = y;
}
/*
Returns whether a point falls within this circle
@param 		x 	X-Coord in question
@param 		y 	Y-Coord in question
@return 	hit  	Boolean 
*/
Circle.prototype.contains = function(x, y) {
	return (this.x + this.radius >= x) && (this.x - this.radius <= x) &&
	       (this.y + this.radius >= y) && (this.y - this.radius <= y);
}

