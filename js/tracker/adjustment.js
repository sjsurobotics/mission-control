
var borderOffset = 5;
var arrowOffset = 10;


function Adjustment(x, y, width, height, ctx) {
	this.x = x;
	this.y = y;
	this.ctx = ctx;
	this.clickedLeft = false;
	this.clickedRight = false;
	this.width = width/2 - 2 * adjustmentOffset;
	this.height = height/2 - 2 * adjustmentOffset;
}

Adjustment.prototype.contains = function(e) {
	var x = e.offsetX;
	var y = e.offsetY;
	return 	(this.x + this.width >= x) && (this.x <= x) &&
			(this.y + this.height >= y) && (this.y <= y);
}

Adjustment.prototype.sideClicked = function(e) {
	var x = e.offsetX;
	var y = e.offsetY;
	var left = (x < this.x + this.width/2);
	var right = (x > this.x + this.width/2);
	return { left: left, right: right};
}

Adjustment.prototype.draw= function() {

	//Draw containing box
	this.ctx.beginPath();
	this.ctx.fillStyle = "LightSteelBlue";
	this.ctx.rect(this.x, this.y, this.width, this.height);
	this.ctx.fill();

	this.ctx.lineWidth = 2;

	//Draw arrow boxes
	if(this.clickedLeft)
		this.ctx.fillStyle = "#FFFFFF";
	else
		this.ctx.fillStyle = "PapayaWhip";
	this.ctx.fillRect(this.x + borderOffset, this.y+borderOffset, this.width/2 - 2 * borderOffset, this.height - 2 * borderOffset);
	if(this.clickedRight) 
		this.ctx.fillStyle = "#FFFFFF";
	else 
		this.ctx.fillStyle = "PapayaWhip";	
	this.ctx.fillRect(this.x + this.width/2+ borderOffset, this.y+borderOffset, this.width/2 - 2 * borderOffset, this.height - 2 * borderOffset);
	
	//Draw arrows
	this.ctx.beginPath();
	this.ctx.strokeStyle = "#444444"
	this.ctx.moveTo(this.x +  this.width/ 3, this.y + borderOffset + arrowOffset);
	this.ctx.lineTo(this.x + this.width/8, this.y + this.height/2);
	this.ctx.lineTo(this.x +  this.width/ 3, this.y + this.height - borderOffset - arrowOffset);
	this.ctx.stroke();

	this.ctx.beginPath();
	this.ctx.moveTo(this.x + this.width * 2/3, this.y + borderOffset + arrowOffset);
	this.ctx.lineTo(this.x + this.width * 7/8, this.y + this.height/2);
	this.ctx.lineTo(this.x + this.width * 2/3, this.y + this.height - borderOffset - arrowOffset);
	this.ctx.stroke(); 
}