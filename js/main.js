"use strict";

// Use mustache.js style templating
_.templateSettings = {
	interpolate: /\{\{(.+?)\}\}/g, // {{ insert_var }}
	escape: /\{\{\{(.+?)\}\}\}/g // {{{ escape_var }}}
};

String.prototype.contains = function(it) { return this.indexOf(it) != -1; };

var app = new Scape();

app.router.config("#content",
[
	{
		url: 'home',
		tmpl: 'pages/home.html',
		js: 'js/pages/home.js'
	},
	{
		otherwise: 'home'
	}
]);

app.router.enable();