var map;
var rocks;
var centerPos;
var roverPos;

var rockStack = [];
var last
var N = 1;
var W = -1;
var E = 1;
var S = -1;

var GPSUpdateInterval;
var DistanceTable;
var line;
var map_socket;
var hardAreas;

var rock_width = 15;
var rover_width = 40;
var COLORS = {
	"purple" : 5,
	"blue" : 4,
	"green" : 3,
	"yellow" : 2,
	"orange" : 1,
	"red"   :  6
}

function initMap() {
	map_socket = io('http://kammce.io:8085/map', {
		forceNew: true,
	    reconnect: false
	});
	map_socket.emit("MAP", "Poop");
	centerPos = new google.maps.LatLng(29.564887, -95.081316);
	var minZoomLevel = 20;
	var mapOpts = {
		center : centerPos,
		zoom : 20,
		zoomControl: true,
		//maxZoom : 19,
		panControl : true,
		mapTypeId : google.maps.MapTypeId.SATELLITE,
		streetViewControl : false,
		draggable : true
	};
	map = new google.maps.Map(document.getElementById("map_div"), mapOpts);

	// Limit the zoom level
   google.maps.event.addListener(map, 'zoom_changed', function() {
     //if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
   });

   var image = {
   	url : 'http://kammce.io/images/rover/images/arrow.png',
   	scaledSize: new google.maps.Size(rover_width,rover_width),
   	anchor: new google.maps.Point(rover_width/2, rover_width/2)
   }
   var icon = {
	   	path: 'M 0,10 5,-5 0,-2 -5,-5 0,10 z',
	    fillColor: 'white',
	    fillOpacity: 1,
	    scale: 2,
	    strokeColor: 'black',
	    strokeWeight: 2,
	    rotation: -45
   }
   roverPos = new google.maps.Marker({
   		map : map,
   		position : centerPos,
   		draggable : true,
   		icon : icon,
   		title : "ROVER " + centerPos.lat() + " " + centerPos.lng()
   });

   google.maps.event.addListener(roverPos, 'drag', function() {
   		
   		center = [roverPos.getPosition().lat(), roverPos.getPosition().lng()];
   });

   google.maps.event.addListener(roverPos, 'dragend', function() {
   		//DistanceTable.recalculateMatrix();
		drawPath();
   		center = [roverPos.getPosition().lat(), roverPos.getPosition().lng()];
   });

   
   var rockFieldCoords = [
    new google.maps.LatLng(29.564828870384844, -95.08164250960527),
    new google.maps.LatLng(29.56475421499836, -95.08161836972414),
    new google.maps.LatLng(29.564707555353806,-95.08170420041262),
    new google.maps.LatLng(29.564766794371046, -95.08177682456835),
    new google.maps.LatLng(29.5648343980613, -95.08174288510241)
  ];

  var rockField = new google.maps.Polygon({
    paths: rockFieldCoords
  });

  var crater1coords = [
  	new google.maps.LatLng(29.564932383166898, -95.0810750150577),
  	new google.maps.LatLng(29.56494410080165, -95.08114918765887),
  	new google.maps.LatLng(29.564999177778645, -95.08117044081865),
  	new google.maps.LatLng(29.565036505388186, -95.0811060678023),
  	new google.maps.LatLng(29.56499451182643, -95.08104705920397)
  ];

  var crater1 = new google.maps.Polygon({
  	paths : crater1coords
  });

  var crater2coords = [
  	new google.maps.LatLng(29.56515634876563, -95.08095431565204),
  	new google.maps.LatLng(29.565156401513043, -95.08101507720812),
  	new google.maps.LatLng(29.565204479460665, -95.08104437699495),
  	new google.maps.LatLng(29.565244139964726,-95.0809987794417),
  	new google.maps.LatLng(29.565211478374216, -95.08094781747042)
  ];

  var crater2 = new google.maps.Polygon({
  	paths: crater2coords
  })

  var crater3coords = [
  	new google.maps.LatLng(29.564922189540262, -95.08085930457293),
  	new google.maps.LatLng(29.564968849085655,-95.0809639107245),
  	new google.maps.LatLng(29.565083164880836,-95.08096659293352),
  	new google.maps.LatLng(29.565144683902947,-95.08086580275454),
  	new google.maps.LatLng(29.565099495698124, -95.08079493155657),
  	new google.maps.LatLng(29.564986094384423, -95.08077367839678)
  ];

  var crater3 = new google.maps.Polygon({
  	paths : crater3coords
  })

  hardAreas = [rockField];

   GPSUpdateInterval = setInterval(updateRoverPosition, 1000);
   DistanceTable = new Table();
   DistanceTable.addRock(new Rock(roverPos, 0, 10, false, "rover"));


}

function updateRoverPosition() {
	var data = sensor.get();
	var icon = {
	   	path: 'M 0,10 5,-5 0,-2 -5,-5 0,10 z',
	    fillColor: 'white',
	    fillOpacity: 1,
	    scale: 2,
	    strokeColor: 'black',
	    strokeWeight: 2,
	    rotation: data["compass"]["heading"] - 180
   }
	roverPos.setIcon(icon);
	//console.log("Received GPS data: " + "Lat: " + data["GPS"]["latitude"] + 
					//data["GPS"]["latitude_dir"] +" Long: " + data["GPS"]["longitude"] + data["GPS"]["longitude_dir"]);
	var lat, lng;
	if(data["GPS"]["latitude"].length > 0) {
		lat = parseGPS(data["GPS"]["latitude"], data["GPS"]["latitude_dir"]);
		lng = parseGPS(data["GPS"]["longitude"], data["GPS"]["longitude_dir"]);
		
	} else {
		lat = 29.564887;
		lng = -95.081316;
	}
	var pos = new google.maps.LatLng(lat, lng);
	if($("#follow-btn").attr("value") == "true")
		map.panTo(pos);
	updateMarker(pos);
	broadcastMap();
}

function parseGPS(value, dir) {
	var multiplier;
	if(dir == "N" || dir == "E") {
		multiplier = 1;
	} else {
		multiplier = -1;
	}
	var degrees = Math.floor(value/100);
	var minutes = value - degrees * 100;
	var degrees = degrees + minutes/60;
	return degrees * multiplier;
}
function updateMarker(pos) {

	roverPos.setPosition(pos);
	//DistanceTable.recalculateMatrix();
	drawPath();
	center = [roverPos.getPosition().lat(), roverPos.getPosition().lng()];
}


function addMarker(point, dist, color) {
	//if(DistanceTable.mat.length > 6) return;
	var LatLng = new google.maps.LatLng(point[0], point[1]);
	console.log(color);

	//var image = 'js/tracker/diamond.png';
	var image = {
	    url: 'http://kammce.io/images/rover/images/Rock_' + color + '.png',
	    scaledSize: new google.maps.Size(rock_width, rock_width),
	    anchor: new google.maps.Point(rock_width/2, rock_width/2)
  	};

	var marker = new google.maps.Marker({
		map : map,
		position : LatLng,
		draggable : true,
		icon : image,
		title : "Rock @ " + LatLng.lat() + " " + LatLng.lng() + "\nDistance: " + dist
	});

	var isHard = checkHardSpots(LatLng);
	
	var rock = new Rock(marker, dist, COLORS[color],isHard, color);
	DistanceTable.addRock(rock);
	drawPath();

	rockStack[rockStack.length] = marker;

	google.maps.event.addListener(marker, 'dblclick', function() {
		DistanceTable.removeRock(marker);
		for(var i = 0; i < rockStack.length; i++)
		{
			if(rockStack[i].getPosition() == marker.getPosition())
			{
				rockStack.splice(i,1);
				break;
			}
		}
		drawPath();
		marker.setMap(null);
		broadcastMap();
	});

	google.maps.event.addListener(marker, 'click', function() {
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
	});
	google.maps.event.addListener(marker, 'drag', function() {
			
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
	});
	google.maps.event.addListener(marker, 'dragend', function() {
		rock.isHard = checkHardSpots(rock.getPosition());
		DistanceTable.dirty = true;
		//DistanceTable.recalculateMatrix();
		drawPath();
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
		broadcastMap();
	});

	google.maps.event.addListener(marker, 'mouseover', function() {
		document.getElementById("distance").innerHTML = "Distance: " + dist;
		document.getElementById("lat").innerHTML = "Lat: " + marker.getPosition().lat();
		document.getElementById("long").innerHTML ="Long: " + marker.getPosition().lng();
	});
	broadcastMap();
	
}
function broadcastMap()
{
	var path = DistanceTable.shortestPath();
	var meta_path =[];
	var data = sensor.get();
	var meta_data = [path[0].lat(), path[0].lng(), path[0].color, data["compass"]["heading"] - 180];
	meta_path[meta_path.length] = meta_data;
	for(var i = 1; i < path.length; i++)
	{
		var meta_data = [path[i].lat(), path[i].lng(), path[i].color];
		meta_path[meta_path.length] = meta_data;
	}

	map_socket.emit("MAP", {directive:"SET", info: meta_path});
	console.log("Emiting map");
}
function checkHardSpots(latLng) {
	var isHard = false;
	for(var i = 0; i < hardAreas.length; i++) {
		isHard = isHard ? isHard : google.maps.geometry.poly.containsLocation(latLng, hardAreas[i]);
	}
	//console.log("New rock is hard? " + isHard);
	return isHard;
}
function drawPath() {

	var path = DistanceTable.shortestPath();
	//console.log("Array of nodes as per Distance.path: ",DistanceTable.getPath());
	//console.log("Array of nodes as per shortestPath(): ", path);

	if(line) line.setMap(null);

	var coordinates = [];
	for(var i = 0; i < path.length; i++) {
		coordinates[coordinates.length] = path[i].getPosition();
	}
	line = new google.maps.Polyline({
		path : coordinates,
		geodisc : true,
		strokeColor : "#FFFF00",
		stokeOpacity : 1,
		strokeWeight : 2
	});
	line.setMap(map);

}