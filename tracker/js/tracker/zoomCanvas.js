

var zoom;
var zCanvas
var zCtx;

var slider;
var slideBarLength;
var slideBarWidth;
var sliderRadius;
var sliderOffset;
var sliderGrabbed = false;

function initZoom() {
	zCanvas = document.getElementById("zoomCanvas");
	zCtx = zCanvas.getContext("2d");
	zoom = 1;

	sliderOffset = 30;
	slideBarLength = panelWidth - 2 * sliderOffset;
	sliderRadius = 18;
	slideBarWidth = 10;

	zCtx.font = "bold 50px sans-serif";
	zCtx.textAlign = "center";
	zCtx.textBaseline = "top";
	zCtx.fillStyle = "Teal";
	zCtx.fillText("ZOOM", panelWidth/2, adjustmentOffset);
	slider = new Slider(panelWidth/2, panelHeight * 2/3, zCtx);
	slider.draw();

	zCanvas.addEventListener("mousedown", ZoomMouseDownListener, false);
	zCanvas.addEventListener("mousemove", ZoomMouseMoveListener, false);
	zCanvas.addEventListener("mouseup", ZoomMouseUpListener, false);
}

function drawSlideBar() {
	zCtx.beginPath();
	zCtx.moveTo(sliderOffset, panelHeight * 2 /3);
	zCtx.lineWidth = slideBarWidth;
	zCtx.lineCap = "round";
	zCtx.strokeStyle = "LightSteelBlue";
	zCtx.lineTo(sliderOffset + slideBarLength, panelHeight * 2/3);
	zCtx.stroke();
}

function ZoomMouseMoveListener(e) {
	if(sliderGrabbed) {
		var x = e.layerX;
		var y = e.layerY;
		slider.move(x,y);
	}
}

function ZoomMouseUpListener(e) {
	sliderGrabbed = false;
}

function ZoomMouseDownListener(e) {
	var offset = $(this).offset();

	var x = e.layerX - offset.left;
	var y = e.layerY - offset.top;
	console.log(x, y);
	if(slider.contains(x,y)) {
		sliderGrabbed = true;
	}
}

function Slider(x, y, ctx) {
	this.x = x;
	this.y = y;
	this.ctx = ctx;
	this.baseRadius = sliderRadius;
	this.radius = sliderRadius;
	this.zoom = 1;
	this.notchWidth = slideBarLength / 7 ;
}

Slider.prototype.draw = function() {
	zoom = this.zoom;
	this.ctx.clearRect(0, this.y - this.radius - 5, panelWidth, panelHeight);
	drawSlideBar();

	this.ctx.beginPath();
	this.ctx.fillStyle = "PapayaWhip";
	this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
	this.ctx.closePath();
	this.ctx.fill();
	this.ctx.strokeStyle = "Teal";
	this.ctx.lineWidth = 2;
	this.ctx.stroke();

	this.ctx.font = "bold 20px sans-serif";
	this.ctx.textAlign = "center";
	this.ctx.textBaseline = "middle";
	this.ctx.fillStyle = "Teal";
	this.ctx.fillText(this.zoom + "x", this.x , this.y);
	setZoom();

}

Slider.prototype.move = function(x, y) {
	x = Math.max(x, sliderOffset);
	this.x = Math.min(x, panelWidth - sliderOffset);
	var pos = x - sliderOffset;
	var notch = Math.floor(pos/this.notchWidth);
	switch(notch) {
		case 0:
			this.zoom = 1;
			this.radius = this.baseRadius;
			break;
		case 7:
			this.zoom = 30;
			this.radius = this.baseRadius + 3 * 6;
			break;
		default:
			this.zoom = 5 * notch;
			this.radius = this.baseRadius + 3 * notch;
			break;
	}
	this.draw();
}

Slider.prototype.slide = function(notch) {
	switch(notch) {
		case 0:
			this.zoom = 1;
			this.radius = this.baseRadius;
			break;
		case 7:
			this.zoom = 30;
			this.radius = this.baseRadius + 3 * 6;
			break;
		default:
			this.zoom = 5 * notch;
			this.radius = this.baseRadius + 3 * notch;
			break;
	}
	this.draw();
}


Slider.prototype.contains = function(x, y) {
	return 	(x <= this.x + this.radius) && (this.x - this.radius <= x) &&
			(y <= this.y + this.radius) && (this.y - this.radius <= y);
}