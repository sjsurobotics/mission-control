/**
*This script manages converting input from the Pitch GUI into
*actual pitch values
*/

var pitch = 0;

var pitchCanvas;
var pCtx;
var pitchDial;
var pitchAdjustment;
var pitchSpinInterval;
var pitchSpinAngle = new Number();

function initPitch() {
	pitchCanvas = document.getElementById("pitchCanvas");
	pitchCanvas.width = panelWidth;
	pitchCanvas.height = panelHeight;
	pCtx = pitchCanvas.getContext("2d");
	
	pitchCanvas.onselectstart = function () { return false; }  //Cancels double clicking
	// pitchCanvas.addEventListener("mouseup", pitchMouseUpListener, false);
	// pitchCanvas.addEventListener("mousedown", pitchMouseDownListener,false);

	
	pitchDial = new Dial(dialX, dialY, dialRadius, dialInnerRadius, pCtx, 0);
	pitchDial.draw();

	// pitchAdjustment = new Adjustment(panelWidth/2 + adjustmentOffset, panelHeight/2 + adjustmentOffset, panelWidth, panelHeight, pCtx)
	// pitchAdjustment.draw();

	displayPitch();
}

function getPitch() {
	return pitchSpinAngle;
}


function displayPitch() {
	pCtx.clearRect(panelWidth/2 + adjustmentOffset, adjustmentOffset, panelWidth/2, panelHeight - 2 *adjustmentOffset);
	pCtx.font = "bold 30px sans-serif";
	pCtx.fillStyle = "Teal"
	pCtx.textAlign = "center";
	pCtx.textBaseline = "top";
	pCtx.fillText("PITCH:", panelWidth * 3/4 , panelHeight/4 );
	pCtx.textBaseline = "bottom"
	pCtx.fillText((pitchSpinAngle).toFixed(2), panelWidth * 3/4, 3*panelHeight/4 );
}

function pitchMouseDownListener(e) {
	if(!registered) return;
	if(pitchAdjustment.contains(e)) {
		var side = pitchAdjustment.sideClicked(e);
		pitchAdjustment.clickedLeft = side.left;
		pitchAdjustment.clickedRight = side.right;
		pitchAdjustment.draw();
		if(side.left) {
			if(pitchSpinAngle > dtheta)
				pitchSpinAngle -= dtheta;
		} else {
			if(pitchSpinAngle < 180 - dtheta)
				pitchSpinAngle += dtheta;
		}
		pitchSpin();
	}
}
function pitchMouseUpListener(e) {
	if(!registered) return;
	pitchAdjustment.clickedLeft = false;
	pitchAdjustment.clickedRight = false;
	pitchAdjustment.draw();
	if(touchingPitchDial(e)) {
		var x = e.clientX;
		var y = e.clientY;
		pitchSpinAngle = pitchDial.calculateAngle(x, y); //degrees
		pitchSpinInterval = setInterval(pitchSpin, 10);
	}
}

function touchingPitchDial(e) {
	var x = e.clientX;
	var y = e.clientY;
	return (x <= panelWidth/2) && (x >= panelWidth/4);
}

function pitchSpin() {
	// calculating = true;

	// var dtheta = (pitchSpinAngle - pitchDial.angle)/10;
	// if(Math.abs(pitchDial.angle - pitchSpinAngle) > .01) {
	// 	pitchDial.spin(pitchDial.angle + dtheta);
	// 	displayPitch();
	// } else {
	// 	pitchDial.spin(pitchSpinAngle);
	// 	clearInterval(pitchSpinInterval);
	// 	displayPitch();
	// 	calculating = false;
	// }
	pitchDial.spin(pitchSpinAngle);
	//clearInterval(pitchSpinInterval);
	displayPitch();
	pitch = (pitchDial.angle * 180/Math.PI).toFixed(2);
}

