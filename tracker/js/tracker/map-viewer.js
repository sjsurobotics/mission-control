/*

This is a Script to allow anyone to have a guest view of the Tracker Map.
To use this script:

	(1) Place a div in your HTML with an ID of "tracker-map". 
		All style is left up to you. Make sure to give the div an explicit width and height.
	(2) Put this link into your HTML:
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4ds5XIvWmG6Xa5ElCJM0wFc5QsaKbwYI"></script>

That should do it. 
*/
var map;
var Map;
var centerPos;
var socket_n;
var rock_width = 15; 
/*
For a Guest Map, the only thing the map should know is a list of 
coordinates that represent first, the rover, followed by all the
different colored rocks.

The map should never perform any calculations on rocks. It should
only receive an updated table and display it's contents. That's it.
So a Map object needs to keep track of a Google maps object so to 
update it, and a list of coordinates to display on the map. 
*/

function Map(socket, map) {
	var that = this;
	this.socket = socket;
	
	
	this.map = map;
	var line;
	var markers = [];
	this.socket.on("MAP", function(table)
	{
		console.log("Receiving the map event");
		console.log(table);
		
		//Remove markers from Map
		for(var i = 0; i < markers.length; i++)
		{
			markers[i].setMap(null);
		}
		markers = [];

		//remove line
		if(line) line.setMap(null);

		//Grab the rover first
		var roverPos = new google.maps.LatLng(table[0][0], table[0][1]);
		var roverImage = {
		   	path: 'M 0,10 5,-5 0,-2 -5,-5 0,10 z',
		    fillColor: 'white',
		    fillOpacity: 1,
		    scale: 2,
		    strokeColor: 'black',
		    strokeWeight: 2,
		    rotation: table[0][3]
   		};	

		var roverMarker = new google.maps.Marker({
		   		map : that.map,
		   		position : roverPos,
		   		draggable : false,
		   		icon : roverImage,
		   		title : "ROVER " + roverPos.lat() + " " + roverPos.lng()
		   });
		markers[markers.length] = roverMarker;
		var coordinates = [roverPos];
		for(var i = 1; i < table.length; i++)
		{
			coordinates[coordinates.length] = new google.maps.LatLng(table[i][0], table[i][1]);

			var image = {
			    url: 'http://kammce.io/images/rover/images/Rock_' + table[i][2] + '.png',
			    scaledSize: new google.maps.Size(rock_width, rock_width),
			    anchor: new google.maps.Point(rock_width/2, rock_width/2)
		  	};

			var marker = new google.maps.Marker({
				map : that.map,
				position : coordinates[coordinates.length - 1],
				draggable : false,
				icon : image,
				title : "Rock"
			});

			markers[markers.length] = marker;
		}

		line = new google.maps.Polyline({
			path : coordinates,
			geodisc : true,
			strokeColor : "#FFFF00",
			stokeOpacity : 1,
			strokeWeight : 2
		});
		line.setMap(that.map);
		map.panTo(roverPos);
	});

}


$(function(){
	
	socket_n = io('http://kammce.io:8085/map', {
		forceNew: true,
	    reconnect: false
	});
	console.log("makin, a socket_n!");
	centerPos = new google.maps.LatLng(29.564887, -95.081316);
	var mapOpts = {
		center : centerPos,
		zoom : 19,
		zoomControl: false,
		maxZoom : 19,
		minZoom : 19,
		panControl : false,
		mapTypeId : google.maps.MapTypeId.SATELLITE,
		streetViewControl : false,
		draggable : false
	};
	map = new google.maps.Map(document.getElementById("tracker-map"), mapOpts);
	Map_obj = new Map(socket_n, map);
});
