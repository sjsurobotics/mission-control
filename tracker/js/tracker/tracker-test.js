var options = {
	mode: 'code',
	modes: ['code', 'form', 'text', 'tree', 'view'], // allowed modes
	error: function(err) {
		alert(err.toString());
	}
};
var schemas = {
	MOTOR: {
		angle: 0,
		speed: 0
	},
	ARM: {
		base: 0,
		shoulderL: 0,
		shoulderR: 0,
		elbow: 0,
		wrist: 0,
		speed: 50
	},
	SENSOR: {
		request: "force-update-all"
	},
	TRACKER: {
		pitch: 0,
		yaw: 0,
		speed: 50
	},
	VIDEO: {
		view: "navi",
		res: 400,
		width: 640,
		height: 480,
		fps: 20
	}
};

var sensor = new JSONEditor(document.querySelector("#sensor"), options);
var feedback = {
	log: function(str, obj) {
		if (typeof obj == "undefined") {
			//console.log(str);
			$("#log").prepend(str + "\n");
		} else {
			//console.log(str, obj);
			$("#log").prepend(str + " " + JSON.stringify(obj) + "\n");
		}
		// keep the log on the bottom
		//var textarea = document.getElementById('log');
		//textarea.scrollTop = textarea.scrollHeight;
	}
};
var entity;
var socket = undefined;
var registered = false;
var getSensors = undefined;
//// VIDEO FEED
// var mcanvas = document.getElementById('multi-cam');
// var mctx = mcanvas.getContext('2d');

// mctx.fillStyle = '#CCC';
// mctx.fillText('... WAITING FOR SERVER ...', mcanvas.width / 2 - 60, mcanvas.height / 2);

// Setup the WebSocket connection and start the player
//var mclient = new WebSocket( 'ws://discovery.srkarra.com:9000/' );
//var mclient, mplayer;
//// DOM Event listeners
$("#connect").on("click", function() {
	if(registered) { return; }
	var address = $("#server").val();
	socket = io('http://' + address + ':8085', {
		forceNew: true,
	    reconnect: false
	});
	// ========== CONNECT SIGNAL ========= //
	socket.on('connect', function() {
		feedback.log('SERVER IS AVAILABLE');
		$("#server_status").css("background", "lime");
		// mclient = new WebSocket('ws://' + address + ':9000/');
		// mplayer = new jsmpeg(mclient, {
		// 	canvas: mcanvas
		// });
 	});
	// =========== ROVER SIGNAL =========== //
	socket.on('ROVERSIG', handleRoverSignal);
	// =========== OCULUS SIGNAL =========== //
	socket.on('OCULARSIG', function(data) {
		feedback.log("INCOMING OCULARSIG", data);
	});
	// =========== SERVER SIGNAL =========== //
	socket.on('SERVERSIG', handleServerSignal);
	// =========== DISCONNECT SIGNAL =========== //
	socket.on('disconnect', function() {
		feedback.log('YOU HAVE DISCONNECTED FROM SERVER!');
		$("#core_status").css("background", "grey");
		$("#oculus_status").css("background", "grey");
		$("#captain_status").css("background", "grey");
		$("#tracker_status").css("background", "grey");
		$("#archaeologist_status").css("background", "grey");
		$("#navigator_status").css("background", "grey");
		$("#"+entity+"_status").css("background", "red");
		$("#server_status").css("background", "red");
		registered = false;
	});

	feedback.log("Attempting to connect!");
	// =========== SUBMIT REGISTRATION =========== //
	entity = $("#entity").val();
	socket.emit("REGISTER", {
		entity: entity,
		password: "destroymit"
	});
});
$("#disconnect").on("click", function() {
	if (typeof socket != "undefined") {
		socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Reset"}});
		socket.disconnect();
		socket.io.close();
		socket = null;
		socket = undefined;
		registered = false;
		location.reload();
		
	}
});



$("#gpsCtrl").on("click", createPOI);


$(".color-picker").on("click", function() {
	color = $(this).attr("value");
	$("#gpsCtrl").css('background-color', $(this).css("background-color"));
		
		$("#gpsCtrl").attr("value", $(this).attr("value"));
});

$(".color-picker").on("")

$("#follow-btn").on("click", function() {
	if($(this).attr("value") == "true") {
		$(this).attr("value", "false");
		$(this).css("background-color", "#5E5E6C");
	}
	else {
		$(this).attr("value", "true");
		$(this).css("background-color", "#DFDFE2");
	}
});
function createPOI() {
	//if(!registered) return;
	var data = sensor.get();
	socket.emit("CTRLSIG", {directive: "TRACKER", info: {req: "Get Range"}});
	//calculateGPS(50, 50, null, "red");
	
	setTimeout(function() {
		data = sensor.get();
		if(data["tracker"]["range"])
		{	calculateGPS(data["compass"]["heading"], data["tracker"]["yaw"], data["tracker"]["range"], $("#gpsCtrl").attr('value')); }
		else
		{	calculateGPS(data["compass"]["heading"], data["tracker"]["yaw"], null, $("#gpsCtrl").attr('value'));}
	}, 1000);
	
}

function handleConnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "lime");
		registered = true;
	} else {
		$("#"+who+"_status").css("background", "lime");
		feedback.log(who+" CONNECTED FROM SERVER");
	}
}

function handleDisconnection(who) {
	if(who == entity) {
		$("#"+entity+"_status").css("background", "red");
		registered = false;
	} else {
		$("#"+who+"_status").css("background", "red");
		feedback.log(who+" DISCONNECTED FROM SERVER");
	}
}

function handleServerSignal(data) {
	feedback.log("INCOMING SERVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "PASSWORD_INCORRECT":
			$("#"+entity+"_status").css("background", "red");
			break;	
		case "CONNECT":
			handleConnection(info);
			break;	
		case "CONNECTIONS":
			handleConnections(info);
			break;
		case "SENSORS":
			handleSensors(info);
			break;
		case "DISCONNECT":
			handleDisconnection(info);
			break;
		default:
			break;
	}
}

function adjustDials() {
	var data = sensor.get();

	yawSpinAngle = data["tracker"]["yaw"];
	yawSpin();

	pitchSpinAngle = data["tracker"]["pitch"];
	pitchSpin();

	rollSpinAngle = data["tracker"]["roll"] || 0;
	rollSpin();
}

function handleRoverSignal(data) {
	//feedback.log("INCOMING ROVERSIG", data);
	var directive = data["directive"];
	var info = data["info"];
	switch(directive) {
		case "SENSORS":
			sensor.set(info);
			adjustDials();
			break;
		default:
			feedback.log("INCOMING ROVERSIG", data);
			break;
	}
}

$(document).unbind('keydown').bind('keydown', function (event) {
    var doPrevent = false;
    if (event.keyCode === 8) {
        var d = event.srcElement || event.target;
        if ((d.tagName.toUpperCase() === 'INPUT' && 
             (
                 d.type.toUpperCase() === 'TEXT' ||
                 d.type.toUpperCase() === 'PASSWORD' || 
                 d.type.toUpperCase() === 'FILE' || 
                 d.type.toUpperCase() === 'EMAIL' || 
                 d.type.toUpperCase() === 'SEARCH' || 
                 d.type.toUpperCase() === 'DATE' )
             ) || 
             d.tagName.toUpperCase() === 'TEXTAREA') {
            doPrevent = d.readOnly || d.disabled;
        }
        else {
            doPrevent = true;
        }
    }
    if (doPrevent) {
        event.preventDefault();
    }
});
