/*
This file will handle parsing user input.
*/

var movingUp = false;
var movingDown = false;
var movingRight = false;
var movingLeft = false;
var acceleration = 1;
var MAX_ACC = 5;

//For each frame that the same arrow key is held down,
//the counter will increase. When the counter increases
//past multiples of frameAmount, pitchVal or yawVal
//start to increase more and more. 
var counter = 0; 
var frameAmount = 15;
function keyUpHandler(e) {
			movingLeft = false; movingRight = false; movingUp = false; movingDown = false;
			counter = 0;
			acceleration = 1;
}


function keyDownHandler(e) {
	
	var pitchVal = 0;
	var yawVal = 0;
	var rollVal =0;
	e.preventDefault();
	switch(e.keyCode) {
		case 49: case 50: case 51: case 52: case 53: case 54: case 55: 
			slider.slide(e.keyCode - 49); 
			zoom = slider.zoom;
			setZoom();
			break;
		case 65:  //left
			if(movingLeft) counter++;
			if(counter%frameAmount == 0 && counter!=0 && acceleration < MAX_ACC) acceleration++;
			movingLeft = true;
			movingRight = false;
			yawVal = acceleration;
			break;
		case 87:  //up
			if(movingUp) counter++;
			if(counter%frameAmount == 0 && counter!=0 && acceleration < MAX_ACC) acceleration++;
			movingUp = true;
			movingDown = false;
			pitchVal = acceleration;
			break;
		case 68:  //right
			if(movingRight) counter++;
			if(counter%frameAmount == 0 && counter!=0 && acceleration < MAX_ACC) acceleration++;
			movingRight = true;
			movingLeft = false;
			yawVal = -acceleration;
			break;
		case 83:  //down
			if(movingDown) counter++;
			if(counter%frameAmount == 0 && counter!=0) acceleration++;
			movingDown = true;
			movingUp = false;
			pitchVal = -acceleration;
			break;
		case 81: //roll left
			rollVal = -1;
			movingDown = false;
			movingUp = false;
			break;
		case 69: //roll right
			rollVal = 1;
			movingDown = false;
			movingUp = false;
			break;
		case 32:   //space
			createPOI();
			break;
		case 70:
			$("follow-btn").trigger("click");
			break;
		default:
			break;
	}
	if(!registered) return;
	var data = getAngles();
	//console.log("Sending angle directives. Pitch: " + pitchVal + " Yaw: "+yawVal + " Roll: " + rollVal + " acceleration: "+ acceleration);
	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Rotations", pitch: pitchVal, yaw: yawVal, roll: rollVal}});
}

/*
GAME PAD  GAME PAD GAME PAD GAME PAD GAME PAD GAME PAD GAME PAD GAME PAD
========================================================================
*/
var colorButtons = document.getElementsByClassName("color-picker");
var curTime = new Date();
var BTN_BOUNCE= 90;
var MIN_TIME_POI = 90;

var selColor = 0;
var AXIS_THRESHOLD = .2;
var gamepads =[];
var start;
var rAF = window.requestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.webkitRequestAnimationFrame;

var rAFStop = window.cancelRequestAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.webkitCancelRequestAnimationFrame;

var notch = 0;

function gamepadHandler(event, connecting) {
  var gamepad = event.gamepad;
  // Note:
  // gamepad === navigator.getGamepads()[gamepad.index]
  console.log("Yo gamepad");
  if (connecting) {
    gamepads[0] = gamepad;
    
    var start = setInterval(gameLoop, 100);
  } 
}

function buttonPressed(b) {
  if (typeof(b) == "object") {
    return b.pressed;
  }
  return b == 1.0;
}

function gameLoop()
{
	// var gamePads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
	// if(!gamePads)
	// 		return;
	var colorButton = document.getElementById("gpsCtrl");
	var pitchVal = 0;
	var yawVal = 0;
	var rollVal =0;
	if(gamepads.length == 0) return;
	var gp = gamepads[0];
	var elapsed = new Date();
	var timeDiff = elapsed - curTime;
	
	//======= Create POI ========
	if(buttonPressed(gp.buttons[1]) && timeDiff > MIN_TIME_POI) {
		
		createPOI();
	}

	//======= Delete POI =======
	if(buttonPressed(gp.buttons[2]) && timeDiff > MIN_TIME_POI && rockStack.length > 0) {
		DistanceTable.removeRock(rockStack[0]);
		rockStack[0].setMap(null);
		rockStack.splice(0, 1);
		drawPath();
		
	}

	//======= Chane COLOR ======
	if(buttonPressed(gp.buttons[0]) && timeDiff > BTN_BOUNCE) {
		console.log("Time diff: " + timeDiff);
		selColor = selColor - 1 < 0 ? colorButtons.length - 1 : selColor -1;
		var button = colorButtons[selColor];
		$("#gpsCtrl").css('background-color', $($('.color-picker')[selColor]).css("background-color"));
		
		colorButton.setAttribute("value", document.getElementsByClassName("color-picker")[selColor].getAttribute("value"));
		

	} else if(buttonPressed(gp.buttons[3]) && timeDiff > BTN_BOUNCE) {
		console.log("Time diff: " + timeDiff);
		selColor = (selColor + 1) % colorButtons.length;
		var button = colorButtons[selColor];
		$("#gpsCtrl").css('background-color', $($('.color-picker')[selColor]).css("background-color"));
		
		colorButton.setAttribute("value", document.getElementsByClassName("color-picker")[selColor].getAttribute("value"));
		
	}
	
	//=======ZOOM=======
	//Check for Zoom adjustments
	if(buttonPressed(gp.buttons[4])) {
		notch = notch - 1 < 0 ? 0 : notch - 1;
		slider.slide(notch);
	}
	else if(buttonPressed(gp.buttons[5])) {
		notch = notch + 1 > 7 ? 7 : notch + 1;
		slider.slide(notch);
	}

	//========PITCH YAW & ROLL========

	//=== PITCH ===
	if(Math.abs(gp.axes[2]) >= AXIS_THRESHOLD) {
		pitchVal = Math.floor(parseInt(gp.axes[2] * 10) / 2);
	} else {
		pitchVal = 0;
	}

	//=== YAW ====
	if(Math.abs(gp.axes[1]) >= AXIS_THRESHOLD) {
		yawVal = -1*Math.floor(parseInt(gp.axes[1] * 10) /2);  //Mult. by -1 because gamepad axis is opposite polar axis
	} else {
		yawVal = 0;
	}
	//Check if second joystick is overriding
	if(Math.abs(gp.axes[3]) >= AXIS_THRESHOLD) {
		yawVal = -1* Math.floor(parseInt(gp.axes[3] * 10) /2) ; //Same here
	}

	//==== ROLL ====
	if(buttonPressed(gp.buttons[7]))
		rollVal = 1;
	else if(buttonPressed(gp.buttons[6]))
		rollVal = -1;

	//Extreme Rotation Mode if both joysticks push horizontally in same direction
	if( (Math.abs(gp.axes[1]) >= .9) && (Math.abs(gp.axes[3]) >= .9) && (gp.axes[1] == gp.axes[3]))
	{
		yawVal = -2 * gp.axes[1]*MAX_ACC;
	}

	//Only send CTRLSIG if there is a yaw command
	if(pitchVal || yawVal || rollVal)
	{
		//console.log("Sending angle directives. Pitch: " + pitchVal + " Yaw: "+yawVal + " Roll: " + rollVal + " acceleration: "+ acceleration);
		socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Rotations", pitch: pitchVal, yaw: yawVal, roll: rollVal}});
	}
	curTime = elapsed;
	
}

window.addEventListener("gamepadconnected", function(e) { gamepadHandler(e, true); }, false);
window.addEventListener("gamepaddisconnected", function(e) { gamepadHandler(e, false); }, false);
