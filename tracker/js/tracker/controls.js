/*
Simple Interface script to get access to the pitch, yaw, zoom and Google map values of the GUI components
*/



function getAngles() {
	//console.log("Begin transmitting angle directives...");
	//while(calculating);
	//console.log("Sending angle directives: Pitch: "+  getPitch() + "  Yaw: "+ getYaw() + "   Roll: " + getRoll());
	return {pitch: getPitch(),
			yaw: getYaw(),
			roll: getRoll()};
}

function deleteCoord() {
	if(typeof gpsX == undefined) return;
	var circleToDelete;
	for(var i = 0; i < circles.length; i++) {
		if(circles[i].contains(gpsX, gpsY)) {
			circleToDelete = circles[i];
			circles.splice(i, 1);
			break;
		}
	}

}

function setZoom() {
	//if(!registered) return;
	var data = getZoom();
	console.log("Sending zoom directives");
	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Set Zoom", zoom: data}});
}
function getZoom() {
	return zoom;
}