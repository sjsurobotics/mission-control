/*
gpsCanvas.js manages the minimap of the Johnson Space Center.
*/



//GPS calculations
var roverLength;
var roverRot = 0; //Rover's roation. Relative to East going counter-clockwise
var center;   	//Rover's GPS coordinates   -- right now it is static. Will need to change once the GPS module is up and running
var coords;		//List of coordinates of interest
var height;		//Height of camera mast (ft), used in trig calculations
var trueYaw;
var drift;
var referenceYaw = 90;

var roverLat;
var roverLong;

function init() {
	roverLat = 29.564887;
	roverLong = -95.081316;
	
	//Initializing the variables above
	center = [roverLat, roverLong];
	height = 3;
	drift = 0;
	
	
	// window.addEventListener("gamepadconnected", function(e) {
 //  console.log("Gamepad connected at index %d: %s. %d buttons, %d axes.",
 //    e.gamepad.index, e.gamepad.id,
 //    e.gamepad.buttons.length, e.gamepad.axes.length);
	// });
}

/*
Calculates the coordinates of a point of interest given the pitch and yaw of the camera mast.
Right now, the function does not take into account the rover's dynamic orientation. Again,
this will be updated when the GPS module is up and running.

@param 	Angle	Value of the rover's current rotation relative to North going clockwise
@param 	Yaw 	Value of the yaw   of the camera mast
@param  Distance Value of the LIDAR reading
@return void    
*/
function calculateGPS(angle, yaw, distance, color) {

	distance = distance || 2000;
	distance /= 100;
	console.log("Calculating GPS coordinate at Yaw: " + yaw + "    Rover Rotation: " + angle  +  "    With drift: " + drift + "     Lidar Reading: " + distance);
	//var dist = height * Math.tan(pitch * Math.PI / 180);    //Old calculation
	var liderReading = distance * 3.28;   //meters to ft
	var dist = Math.sqrt(Math.pow(liderReading,2) - Math.pow(height, 2));
	var theta = 180 - (angle + 90) % 360;
	roverRot = theta < 0 ? theta + 360 : theta;

	
	trueYaw = (yaw - 90) + roverRot + drift;
	//console.log(dist);
	var dLat = dist/(370800) * Math.sin(toRads(trueYaw));  //Using ft.
	var dLong = dist/(370800) * Math.cos(toRads(trueYaw));

	//Here is where we will need to take into account the rover's orientation
	var point = [center[0] + dLat, center[1] + dLong];
	addMarker(point, dist, color);
	//console.log("Coordinate point: " + point[0] + " " + point[1]);
	
}


function setDrift(){
	drift = referenceYaw - yawSpinAngle;
}





