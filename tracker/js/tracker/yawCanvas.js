/**
*This script manages converting input from the Yaw GUI into
*actual pitch values
*/




var yaw = 0;
var deltaDegree = .5;
var dtheta = .5
var panelWidth = 300;
var panelHeight = 150;
var dialRadius = panelHeight/2;
var dialInnerRadius = 5;

var dialX = panelWidth/4;
var dialY = panelHeight/2;

var yawCanvas;
var yCtx;
var yawDial;
var yawAdjustment;

var yawSpinInterval;
var yawSpinAngle = new Number();

var adjustmentOffset = 5;

function initYaw() {
	yawCanvas = document.getElementById("yawCanvas");
	yawCanvas.width = panelWidth;
	yawCanvas.height = panelHeight;
	yCtx = yawCanvas.getContext("2d");

	yawCanvas.onselectstart = function () { return false; } //cancels double clicking
	
	// yawAdjustment = new Adjustment(panelWidth/2 + adjustmentOffset, panelHeight/2 + adjustmentOffset, panelWidth, panelHeight, yCtx);
	// yawAdjustment.draw();

	yawDial = new Dial(dialX, dialY, dialRadius, dialInnerRadius, yCtx, 0);
	yawDial.draw();

	displayYaw();
	
	// yawCanvas.addEventListener("mouseup", yawMouseUpListener, false);
	// yawCanvas.addEventListener("mousedown", yawMouseDownListener, false);
	window.addEventListener("keydown", keyDownHandler, false);
	window.addEventListener("keyup", keyUpHandler, false);
}



function incrementYaw() {
	yawSpinAngle += dtheta;
	yawSpin();
}

function decrementYaw() {
	yawSpinAngle -= dtheta;
	yawSpin();
}


function emitAnglesRequest() {
	if(!registered) return;


	socket.emit("CTRLSIG", {directive: "TRACKER", info:{req: "Get Rotations"}});


	var data = sensor.get();
	console.log(sensor);
	calculateGPS(data["compass"]["heading"], data["tracker"]["yaw"]);
}
function getYaw() {
	return yawSpinAngle;
}

function displayYaw() {
	yCtx.clearRect(panelWidth/2 + adjustmentOffset, adjustmentOffset, panelWidth/2, panelHeight - 2 *adjustmentOffset);
	yCtx.font = "bold 30px sans-serif";
	yCtx.textAlign = "center";
	yCtx.textBaseline = "top";
	yCtx.fillStyle = "Teal";
	yCtx.fillText("YAW:", panelWidth * 3/4 , panelHeight/4 );
	yCtx.textBaseline = "bottom";
	yCtx.fillText(yawSpinAngle.toFixed(2), panelWidth * 3/4, 3*panelHeight/4  );
}

function yawMouseDownListener(e) {
	if(!registered) return;
	if(yawAdjustment.contains(e)) {
		var side = yawAdjustment.sideClicked(e);
		yawAdjustment.clickedLeft = side.left;
		yawAdjustment.clickedRight = side.right;

		if(side.left) {
			if(yawSpinAngle < 180 - dtheta)
				yawSpinAngle += dtheta;
		} else {
			if(yawSpinAngle > dtheta)
				yawSpinAngle -= dtheta;
		}
		yawSpin();
		yawAdjustment.draw();
	}
}
function yawMouseUpListener(e) {
	if(!registered) return;
	yawAdjustment.clickedLeft = false;
	yawAdjustment.clickedRight = false;
	yawAdjustment.draw();
	// if(touchingYawDial(e)){
	// 	var x = e.clientX;
	// 	var y = e.clientY;
	// 	yawSpinAngle = yawDial.calculateAngle(x, y);
	// 	yawSpinInterval = setInterval(yawSpin, 10);
	// } 
}

function yawSpin() {
	
	yawDial.spin(yawSpinAngle);
	displayYaw();
	yaw = (yawDial.angle * 180/Math.PI).toFixed(2);
}



function toDegrees(angle) {
	return angle * 180/Math.PI;
}

function toRads(angle) {
	return angle * Math.PI / 180;
}
