
var roll = 0;
var rollCanvas;
var rCtx;
var rollDial;
var rollAdjustment;

var rollSpinInterval;
var rollSpinAngle = new Number();

function initRoll()
{
	rollCanvas = document.getElementById("rollCanvas");
	rollCanvas.width = panelWidth;
	rollCanvas.height = panelHeight;
	rCtx = rollCanvas.getContext("2d");

	rollCanvas.onselectstart = function () { return false; } //cancels double clicking
	
	// rollAdjustment = new Adjustment(panelWidth/2 + adjustmentOffset, panelHeight/2 + adjustmentOffset, panelWidth, panelHeight, rCtx);
	// rollAdjustment.draw();

	rollDial = new Dial(dialX, dialY, dialRadius, dialInnerRadius, rCtx, 0);
	rollDial.draw();
	rollSpinAngle = 0;
	displayroll();
}

function getRoll() {
	return rollSpinAngle;
}

function displayroll() {
	rCtx.clearRect(panelWidth/2 + adjustmentOffset, adjustmentOffset, panelWidth/2, panelHeight - 2 *adjustmentOffset);
	rCtx.font = "bold 30px sans-serif";
	rCtx.textAlign = "center";
	rCtx.textBaseline = "top";
	rCtx.fillStyle = "Teal";
	rCtx.fillText("ROLL:", panelWidth * 3/4 , panelHeight/4 );
	rCtx.textBaseline = "bottom";
	rCtx.fillText(rollSpinAngle.toFixed(2), panelWidth * 3/4, 3*panelHeight/4 );
}

function rollMouseDownListener(e) {
	if(!registered) return;
	if(rollAdjustment.contains(e)) {
		var side = rollAdjustment.sideClicked(e);
		rollAdjustment.clickedLeft = side.left;
		rollAdjustment.clickedRight = side.right;

		if(side.left) {
			if(rollSpinAngle < 180 - dtheta)
				rollSpinAngle += dtheta;
		} else {
			if(rollSpinAngle > dtheta)
				rollSpinAngle -= dtheta;
		}
		rollSpin();
		rollAdjustment.draw();
	}
}
function rollMouseUpListener(e) {
	if(!registered) return;
	rollAdjustment.clickedLeft = false;
	rollAdjustment.clickedRight = false;
	rollAdjustment.draw();
	if(touchingrollDial(e)){
		var x = e.clientX;
		var y = e.clientY;
		rollSpinAngle = rollDial.calculateAngle(x, y);
		rollSpinInterval = setInterval(rollSpin, 10);
	} 
}

function rollSpin() {
	
	rollDial.spin(rollSpinAngle);
	displayroll();
	roll = (rollDial.angle * 180/Math.PI).toFixed(2);
}