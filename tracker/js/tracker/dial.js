/**
This class is used for animaiting the dial for pitch and yaw
The angle kept by this class is going to be measured in degrees in standard position going clockwise.
Only when the dial is animated and displayed will the angle be measured as the negative radian measure of itself (this is done
beause the Canvas.rotate() method takes in a radian measure, and we want to spin counterclockwise).
*/

function Dial(x, y, radius, innerRadius, ctx, offset) {
	this.x = x;
	this.y = y;
	this.radius = radius;
	this.innerRadius = innerRadius;
	this.angle = 0;
	this.ctx = ctx;
	this.offset = offset;
}

/**
Draws the dial. This is the only method where the angle is viewed as the negative radian measure 
of itself.
*/
Dial.prototype.draw = function() {
	this.ctx.clearRect(0,0, this.radius * 2, this.radius * 2);
	
	this.ctx.save();

	//Dial drawing
	this.ctx.translate(this.x, this.y);
	this.ctx.rotate(this.offset);
	this.ctx.fillStyle = "LightSteelBlue";
	this.ctx.beginPath();
	this.ctx.arc(0, 0, this.radius, 0 , Math.PI);
	this.ctx.closePath();
	this.ctx.fill();

	this.ctx.fillStyle = "PapayaWhip";
	this.ctx.beginPath();
	this.ctx.arc(0, 0, this.radius, Math.PI, Math.PI * 2);
	this.ctx.closePath();
	this.ctx.fill();

	//Needle drawing
	this.ctx.fillStyle = "#000000";
	this.ctx.beginPath();
	this.ctx.arc(0, 0, this.innerRadius, 0, Math.PI * 2);
	this.ctx.closePath();
	this.ctx.fill();

	this.ctx.save();
	this.ctx.rotate(toRads(this.angle * -1));
	this.ctx.beginPath();
	this.ctx.moveTo(0, 0);
	this.ctx.lineTo( this.radius, 0);
	this.ctx.stroke();
	this.ctx.restore();

	this.ctx.restore();
}

/*
Rotates the internal angle and then redraws the dial to that angle.
This method should called in conjunction with a timer to create the illusion of animation
@param   angle    An angle in degrees.
*/
Dial.prototype.spin = function(angle) {
	this.angle = angle;
	this.draw();
}

/**
Calculates the appropriate degree measure from 0 - 180 according to this.offset
given a mouse offset x and a mouse offset y.
@param  mx    Mouse offset x
@param  my	  Mouse offset y
@return angle  The adjusted degree measure for this dial
*/
Dial.prototype.calculateAngle = function(mx, my) {
	var x = mx - this.x;
	var y = my - this.y;
	var angle = toDegrees(Math.atan(y/x));
	if(x < 0) {
		angle = 180 - angle;
	}
	if(angle < 0) {
		angle *= -1;
		angle += toDegrees(this.offset);
	}
	if(y >= 0) {
		angle = 90 - angle;
	}
	console.log("Calculated angle is: " + angle);
	return Math.max(Math.min(180, angle) ,0);
}