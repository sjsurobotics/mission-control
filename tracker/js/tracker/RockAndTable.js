
/*
A Rock is used to sotre GPS information, distance away from the rover,
point value, and wheter it is in a hard spot. 
@param  latLng    A google.maps.LatLng() object holding GPS info
@param  distance  Distance away from the rover at time of discovery
*/
function Rock(marker, dist, value, isHard, color) {
	this.marker = marker;
	this.latLng = marker.getPosition();
	this.dist = dist;
	this.value = value;
	this.isHard = isHard;
	this.color = color;
	this.lng = function() {
		return this.marker.getPosition().lng();
	};
	this.lat = function() {
		return this.marker.getPosition().lat();
	}
}

Rock.prototype.getPosition = function() {
	return this.marker.getPosition();
}


/*
A Table is used as a matrix that holds the data for all the Adjacency list
The table always comes of the form

      |Rover |  Rock1 | Rock2 
------|-------------- |--------
Rover |   0  |    1.2 |   1.1
------|-------------- |--------
Rock1 |  1.2 |    0   |   2.1
------|-------------- | --------
Rock2 |  .9  |   1.1  |    0 

where edge (i, j) does not always equal (j, i)

Edge (i, j) generally represents the time-point cost of getting the rover
from the coordinates of Vi to obtaining a rock at coordinates of Vj.
This value is a function that increases with the latitudinal-longitudal difference 
between the two coordinates, decreases with rock point value, and increases if
the rock is in a hard lat-lng position. Given the distance, point value, and boolean
indicator isHard, the function that determines the time-cost value to get from
Vi to Vj is

f(distance, point value, isHard) = dist - (point value)^1.5/dist + isHard * dist

*/
function Table() {
	//mat will act as edge weight matrix
	this.mat = new Array();

	//vertices will hold all the points of interests (Rocks) and the roverpos
	this.vertices = new Array();
	this.path = new Array();
	this.dirty = false;

	//var path = new Array();

	this.recalculateMatrix = function() {
		for(var i = 0; i < that.vertices.length;i++) {
			for(var j = 0; j < that.vertices.length; j++) {
				that.mat[i][j] = calculateEdgeWeight(that.vertices[i], that.vertices[j]);
			}
		}
	}

	//Recursive, public helper method
	this.shortestPath = function() {
		//if(!that.dirty) return that.path;
		var queue = copyArray(that.vertices);
		var bestPath = [queue[0]];
		var from = queue[0];
		queue.splice(0,1);
		while(queue.length > 0)
		{
			var bestRock = queue[0];
			var bestIdx = 0;
			var i = 0;
			while(i < queue.length && queue[i].value == bestRock.value)
			{
				if(calculateDistance(from, queue[i]) < calculateDistance(from, bestRock))
				{
					bestRock = queue[i];
					bestIdx = i;
				}
				i++;
			}
			bestPath[bestPath.length] = bestRock;
			from = bestRock;
			queue.splice(bestIdx, 1);
		}
		that.path = copyArray(bestPath);
		that.dirty = false;
		return bestPath;

		// var curPath = [];
		// curPath = calcShortestPath(0,0,curPath);
		// var nodes = [];
		// for(var i = 0; i < curPath.length; i++) {
		// 	nodes[nodes.length] = that.vertices[curPath[i]];
		// }
		// return nodes;
	}
	function copyArray(arr)
	{
		var copy = [];
		for(var i = 0; i < arr.length; i++)
		{
			var o = that.vertices[i];
			copy[copy.length] = new Rock(o.marker, o.dist, o.value, o.isHard, o.color);
		}
		return copy;
	}
	this.getPath = function() {
		return path;
	}

	this.removeRock = function(marker) {
		var idx = -1;
		for(var i = 0; i < that.vertices.length; i++) {
			if(that.vertices[i].getPosition() == marker.getPosition())
				idx = i;
		}
		that.vertices.splice(idx, 1);

		for(var i = 0; i < that.mat.length; i++) {
			that.mat[i].splice(idx, 1);
		}
		that.mat.splice(idx, 1);
		that.dirty = true;
	}

	/*
	Calcuates the cost of a path.
	Path argument holds the order of the INDEXES of the rocks
	that make up the path. These indexes correspond to both
	this.vertices and this.mat. 
	*/
	function calcPathCost(path) {
		var cost = 0;
		for(var i = 0; i < path.length - 1; i++) {
			cost += that.mat[path[i]][path[i+1]];
		}
		return cost;
	}

	/*
	Private recursive shortest path method
	*/
	function calcShortestPath(curDist, node, curPath) {
		//Visited nodes is a list of all the rock numbers that we've used in
		//this iteration of the method, such as [0, 2, 3], meaning the rover, 
		//the second rock, and the third rock. To access rock info, you should 
		//call this.vertices[i].
		if(curPath.length == that.vertices.length - 1) {
			curPath[curPath.length] = node;
			return curPath;
		}

		curPath[curPath.length] = node;

		var tmpDist = curDist;
		var tmpVisited = curPath.slice();
		var bestPath = [];
		var bestCost = 0;

		for(var i = 1; i < that.vertices.length; i++) {
			if(curPath.indexOf(i) < 0) {

				//Recursive call. Compare each path going through each subsequent
				//node.
				var path = calcShortestPath(tmpDist + that.mat[node][i],  //mat[node][i] = weight of (node, vert[i])
													i, tmpVisited);
				if(bestPath.length == 0 || bestCost > calcPathCost(path)) {
					bestPath = path;
					bestCost = calcPathCost(path);
				}

				//Reset tmp list of vertices
				tmpVisited = curPath.slice();
			}
		}

		that.path = bestPath;
		return bestPath;
	}

	var that = this;
}


Table.prototype.addRock = function(rock) {
	
	// this.vertices[this.vertices.length] = rock;
	// this.vertices.sort(function(a, b) {
	// 	return b.value - a.value;
	// });
	// this.dirty = true;
	// return;
	var i = 0;
	var done = false;
	while(i < this.vertices.length && !done)
	{
		if(this.vertices[i].value >= rock.value) {
			i++;
		} else {
			done = true;
		}
	}
	this.vertices.splice(i, 0 , rock);
	this.dirty = true;


	// //Create an adjacency row for this rock
	// var edgeWeights = [];
	// for(var i = 0; i < this.vertices.length; i++) {
	// 	edgeWeights[edgeWeights.length] = calculateEdgeWeight(rock, this.vertices[i]);
	// }
	// edgeWeights[edgeWeights.length] = 0; //Add a zero for the edge (r, r) in the adj. list

	// //Update the last column in all the existing adjaceny lists
	// for(var i = 0; i < this.mat.length; i++) {
	// 	this.mat[i][this.mat[i].length] = calculateEdgeWeight(this.vertices[i], rock);
	// }

	// //Add the new rock to the list of vertices
	// //and add in this new row in the adjacency matrix
	// this.vertices[this.vertices.length] = rock;
	// this.mat[this.mat.length] = edgeWeights;
}

/*
Calculates the edge weight between two Rock objects
*/
function calculateEdgeWeight(src, dest) {

	//Calc distance between each vertex
	var dist, weight;
	dist = weight = calculateDistance(src, dest);

	if(dest.value) {
		//Let rock value be proportional to how far away the rock is
		weight -= Math.pow(dest.value, 1.5) / dist;

		//Let the rock's hardness be proportional to its distance as well;
		if(dest.isHard) {
			weight += dest.value/dist;
		}
	}
	return weight;
}
/*
Calculates the distance between two Rocks in terms of latitude.
This result will be insanely small. 
*/
function calculateDistance(v1, v2) {
	return Math.sqrt(Math.pow(v1.getPosition().lng() - v2.getPosition().lng(), 2) + Math.pow(v1.getPosition().lat() - v2.getPosition().lat(), 2));
}