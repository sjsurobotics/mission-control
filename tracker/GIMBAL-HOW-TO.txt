GAMEPAD CONTROLS FOR TRACKER SYSTEM

RUN THE TRACK-TEST.HTML ON FIREFOX ONLY

There are 3 areas of functionality for the tracker:

(1) Pitch, Yaw, And Roll

(2) Camera Zoom

(3) Creating Rocks on the Mini-Map


How to control these areas.

(1) PITCH YAW AND ROLL

Pitch and Yaw are controlled via the joysticks.

The LEFT JOYSTICK can control both pitch and yaw.

	The vertical axis is an inverted control for pitch.

	The horizontal axis:
		Left: Rotates Gimbal in a counterclockwise fashion 
		Right: " "        " in a clockwise fashion

NOTE: There are limits for yaw and pitch. Pitch can't leave the range (-60, 30) degrees
					  Yaw can't leave the range (-180, 180) degrees

The RIGHT JOYSTICK overrides the yaw controls from the left joystick. 

Yaw has the ability to move very quickly. Push both joysticks either fully to the right or fully to the left to have the gimbal spin very quickly. 

	(The speed of this rapid spin can be adjust by going into "input_control.js" and changing the value of MAX_ACC. It is currently set to 5.)


(2) CAMERA ZOOM

Use the controller bumpers to zoom in and out



(3) ADDING ROCKS TO MINIMAP

Press   A   to add rocks to the minimal. You are capped at six rocks per mini-map. 

To change the color of the rock that you will place on the mini-map you can cycle through the buttons atop by pressing X or Y. The color of the "Get coordinates!" button indicates what color rock you will place on the map.

NOTE: Be careful of double rocks appearing. 

Double click a rock to get rid of it. 

A rock can be dragged. 
